$(function(){
	var FOLIOG = 0;
	console.log(ACUERDOG);
	if ( ACUERDOG ){
		$('#modal-acciones').modal('show');
	}

	$('.iradio_minimal, .iCheck-helper').on('click', function(){
		var value = $('input:radio[name=check-acciones]:checked').val();
		if( value == 'si' )
		{
			$('#pregunta').remove();
			$('.formulario').show(500);
		}
		else if(value == 'no')
		{
			$('#modal-acciones').modal('hide');
		}
	});

	$('#guardar-accion').click(function (){
		var value = $('#descripcion-accion').val();
		$('#descripcion-accion').val('');
		if( value != '' )
		{
			guardar_accion (value,false);
		}
		$('#descripcion-accion').focus();
	});
	$('#terminar_acciones').click(function(){
		var value = $('#descripcion-accion').val();
		$('#descripcion-accion').val('');
		if( value != '' )
		{
			guardar_accion (value, true);
		}
	});
	function guardar_accion (value, t) {
		console.log(ACUERDOG);
		var datos =	{value: value, acuerdo: ACUERDOG, folio: $('#acuerdo').val()};
		if($('[name="folio_accion"]').length > 0){
			datos.folio_accion = $('[name="folio_accion"]').val();
		}
		$.ajax({
			url: base_url+'minutas/accion',
			type: 'POST',
			dataType: 'JSON',
			data: datos,
			success: function(resp){
				if(resp.status == 'OK'){
					if( t === false){
						if( resp.update === 0 ){
							$('#acciones > tbody').append('<tr data-id="'+resp.folio+'"><td class="folio">'+FOLIOG+'</td><td>'+value+'</td><td><a class="btn btn-success btn-xs btn-flat editar-accion" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a><a class="btn btn-danger btn-xs btn-flat eliminar-accion" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times "></i></a></td></tr>');
						}else if(resp.update === 1){
							$('#acciones > tbody [data-id='+resp.folio+']').html('<td class="folio">'+$('#num_accion').html()+'</td><td>'+value+'</td><td><a class="btn btn-success btn-xs btn-flat editar-accion" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a><a class="btn btn-danger btn-xs btn-flat eliminar-accion" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times "></i></a></td>');
						}
						$('#folio-accion').html('');
						$('[data-toggle="tooltip"]').tooltip();
					}else{
						location.href = base_url+'minutas';
					}	
				}
			}
		});
		FOLIOG++;
	}
	$('#modal-acciones').on('click', '.eliminar-accion', function(){
		var button 		= $(this),
			tr 			= button.closest('tr'),
			accion_id 	= $(tr).attr('data-id'),
			folio 		= tr.find('.folio').html();
		if( confirm("¿Quieres borrar la Accion: "+folio+"?") )
		{
			$.ajax({
				url: base_url+'minutas/accion_remove',
				type: 'POST',
				dataType: 'JSON',
				data: {accion: accion_id},
				success: function (resp){
					if( resp.status == 'OK' )
					{
						$(tr).remove();
					}
				}
			});
		}
	});

	$('#modal-acciones').on('click', '.editar-accion', function(){
		var button 		= $(this),
			tr 			= button.closest('tr'),
			accion_id 	= $(tr).attr('data-id');
		$('#folio-accion').html('');		
		$('#folio-accion').html('<input type="hidden" name="folio_accion" value="'+accion_id+'"><span id="num_accion"></span>');
		$.ajax({
			url: base_url+'minutas/accion_edit',
			type: 'POST',
			dataType: 'JSON',
			data: {accion: accion_id},
			success: function (resp){
				$('#descripcion-accion').val(resp.descripcion);
				$('#num_accion').html(resp.folio);
				$('#descripcion-accion').focus();
			}
		});
	});

	$('#aceptar-accion').click( function(e){
		$('#modal-acciones').modal('hide');
		$('#acciones > tbody').html('');
		$('#descripcion-accion').val('');
	});	
	$('.more-acciones').click(function(e){
		e.preventDefault();
		var acuerdo = $(this),
			id 		= acuerdo.attr('data-id');
		$('#acciones > tbody').html('');
		$('#descripcion-accion, #folio_acuerdo').val('');
		$('#descripcion-accion').focus();
		$.ajax({
			url: base_url+'minutas/acciones',
			type: 'POST',
			dataType: 'JSON',
			data:{ acuerdo: id},
			success: function(resp){
				if( resp.status === 'OK')
				{
					$('#folio_acuerdo').val(resp.acuerdo.folio);
					$('#acuerdo').val(resp.acuerdo.id);
					ACUERDOG = resp.acuerdo.id;
					$('#pregunta').remove();
					$('.formulario').show(500);
					$('#modal-acciones').modal('show');
					$.each(resp.data, function(i, item){
						$('#acciones > tbody').append('<tr data-id="'+item.id+'"><td class="folio">'+item.folio+'</td><td>'+item.descripcion+'</td><td><a class="btn btn-success btn-xs btn-flat editar-accion" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a><a class="btn btn-danger btn-xs btn-flat eliminar-accion" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times "></i></a></td></tr>');
						FOLIOG = item.folio;
					});
					$('[data-toggle="tooltip"]').tooltip();
				}
			}
		})
	});

	$('#terminar').click(function(){
		$('[name=terminado]').val('t');
		$('#form-acuerdo').submit();
	});

	$('#addoficio').click(function(){
		$('#extras').html('<input type="hidden" name="oficio" value="t">');
		$('#form-acuerdo').submit();
	});

	$(document).on('click', '.delete-acuerdo', function(){
		var button 	= $(this),
			acuerdo_id = button.attr('data-id'),
			tr 		= button.closest('tr'),
			folio 	= tr.find('span').html();
		if (confirm("¿Quieres borrar el acuerdo: "+folio+", con sus acciones?"))
		{
			$.ajax({
				url: base_url+'minutas/acuerdo_remove',
				type: 'POST',
				dataType: 'JSON',
				data: {acuerdo: acuerdo_id},
				success: function (resp){
					if( resp.status == 'OK' )
					{
						$(tr).remove();
					}
				}
			});
		}
	});

	$(document).on('click', '.edit-acuerdo', function(){
		var button = $(this),
			folio_acuerdo = button.attr('data-id');
		$('#folio-acuerdo').html('');
		$('#folio-acuerdo').html('<input type="hidden" name="folio_acuerdo" value="'+folio_acuerdo+'">');
		$.ajax({
			url: base_url+'minutas/acuerdo',
			type: 'POST',
			dataType: 'JSON',
			data: {acuerdo: folio_acuerdo},
			success:function(resp){
				$('#form-acuerdo #folio').val(resp.folio);
				$('#form-acuerdo #descripcion').val(resp.descripcion);
				$('#form-acuerdo #descripcion').focus();
			}
		});
	});
})