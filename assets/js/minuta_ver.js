﻿$(function(){
	$('#acuerdos').dataTable({
		"sDom": "<'row hidden-print'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row hidden-print'<'col-sm-6'i><'col-sm-6'p>>",
		"aaSorting": [],
		"oLanguage": {
	      "sUrl": base_url+"assets/js/spanish.json"
	    }
	});
	$('[data-toggle="tooltip"]').tooltip();
	$(document).on('click', '#apoyo', function(e){
		e.preventDefault();
		$('#modal-apoyo').modal('show')
	});

	$(document).on('click', '.hecha-accion' ,function(e){
		e.preventDefault();
		var button	= $(this),
			celda	= button.closest('td');
			row 	= button.closest('tr');
			accion_id= $(row).attr('data-id');
		$.ajax({
			url: base_url+'minutas/accion_hecha',
			type: 'POST',
			dataType: 'JSON',
			data: {accion: accion_id},
			success: function(resp){
				if(resp.status == 'OK')
				{					
					$(celda).html('<span class="label label-primary">Hecha</span>');
					console.log($('[row='+resp.acuerdo+']').find('.progress'));
					$($('[row='+resp.acuerdo+']').find('.progress')).attr('data-original-title', resp.progress+'%');
					var bar = $('[row='+resp.acuerdo+']').find('.progress-bar');
					$(bar).css('width', resp.progress+'%');
					$(bar).attr('aria-valuenow', resp.progress);
				}
			}
		})
	});

	$(document).on('click', '.eliminar', function(){
		var button 	= $(this),
			acuerdo_id = button.attr('data-id'),
			tr 		= button.closest('tr'),
			folio 	= tr.find('span').html();
		if (confirm("¿Quieres borrar el acuerdo: "+folio+", con sus acciones?"))
		{
			$.ajax({
				url: base_url+'minutas/acuerdo_remove',
				type: 'POST',
				dataType: 'JSON',
				data: {acuerdo: acuerdo_id},
				success: function (resp){
					if( resp.status == 'OK' )
					{
						location.href = base_url+'minutas/ver/'+$(tr).attr('data-minuta');
					}
				}
			});
		}
	});
})