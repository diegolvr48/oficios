$(function(){

	$('#acuerdos').on('click', function(e){
		$('[name="acuerdos"]').val('t');
		$('#form-minuta').submit();
	});

	$('#fecha').datetimepicker({
		pickTime: false
	});
	$('#attachment').change(function (){
		$('#content_img').append('<div class="col-md-2 loading-img"><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div></div>');
		var form = new FormData();
		form.append("attachment", $('#attachment')[0].files[0]);
		form.append("clave", $('#clave').val());
		$.ajax({
			url: base_url+"minutas/imagenes",
			type: "POST",
			dataType: 'JSON',
			data: form, 
			contentType: false,
			cache: false,
			processData:false,
			success: function(response)
			{
				$('#clave').val(response.clave);
				console.log(response.status);
				if( response.status == "OK" )
				{
					console.log(response);
					$('.loading-img').remove();
					$('#content_img').append('<div class="col-md-2"><div class="box box-danger"><div class="box-tools pull-right"><button class="btn btn-danger btn-xs" type="button" data-widget="remove" data-id="'+response.imagen+'"><i class="fa fa-times"></i></button></div>'+
						'<div class="box-body"><img src="'+base_url+'upload/'+response.name+'"></div></div></div>');
				}
			}
		});
	});

	$(document).on('click', '[data-widget="remove"]', function (){
		var element = $(this),
			imagen 	= element.attr('data-id');
		element.closest('.col-md-2').remove();
		$.ajax({
			url: base_url+'minutas/removeImg',
			type: 'POST',
			dataType: 'JSON',
			data: {imagen: imagen},
			success: function(resp){
				console.log(resp);
			}
		});
	});
})