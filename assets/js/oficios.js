$(function(){
	var $tabla_co = $('#oficios').dataTable({
		"sDom": "<'row' <'col-sm-6 nueva'> <'col-sm-6' T>><'row'<'col-sm-6'l><'col-sm-3 filtros-tiempo'><'col-sm-3'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
		"oLanguage": {
	      "sUrl": base_url+"assets/js/spanish.json"
	    },
	    "tableTools": {
	    	"aButtons": [
	    		"xls", 
	    		{
                    "sExtends": "pdf",
                    "sTitle": "Control de Correspondencia",
                    "sPdfMessage": "Listado de correspondencia"
                }
	    	],
            "sSwfPath": base_url+"assets/swf/copy_csv_xls_pdf.swf"
        },
	    "fnInitComplete": function(oSettings, json) {
	    	if( $('#check').length ) 
	    	{$('.nueva').html('<a href="'+base_url+'home/nuevo" class="btn btn-success btn-flat"> Nueva</a>');}
	    	var form = $('#formulario').html();
     		$(".filtros-tiempo").html('<a href="#" title="Filtros" role="button" data-toggle="popover" data-placement="left" data-content="'+form+'">Filtrar <i class="fa fa-filter"></i></a>');
     		$('[data-toggle="popover"]').popover({
     			html: true
     		});
    	}
	});
	$(document).on('click','.eliminar', function(e){
		e.preventDefault();
		var a = $(this),
			id = a.attr('data-id');
		$('#delete-modal').modal('show');
		$('#aceptar').attr('data-id',id);
	});

	$('#aceptar').click(function (){
		var id = $('#aceptar').attr('data-id');
		$('#delete-modal').modal('hide');
		$('#aceptar').removeAttr('data-id');
		$.ajax({
			url: base_url+'home/bajacorrespondencia',
			data: {correspondencia: id},
			dataType: 'JSON',
			type:'POST',
			success: function(resp){
				alert(resp.mensaje);
				location.href = base_url;
			}
		})
	});
});