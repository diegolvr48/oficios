$(function(){
	$(".datemask").datetimepicker({
		pickTime: false
	});
	$(".timepicker").datetimepicker({
		format: 'HH:mm',
		pickDate: false,
		pick12HourFormat: false
	});
	$('#area_turnar').change(function(){
		var area = $(this).val();
		$.ajax({
			url: base_url+"home/titulares",
			data: {area: area},
			dataType: 'JSON',
			type: 'POST',
			success: function(resp){
				$('#titular_area').html('');
				if( resp.status === 'OK')
				{
					$.each(resp.users, function(i, item){
						$('#titular_area').append('<option value="'+item.id+'">'+item.last_name+' '+ item.first_name+'</option>');
					});
				}
			}
		});
	});
	$('#tipo_documento').change(function(){
		var tipo = $(this).val();
		if( tipo != 'informativo' ){
			$('#respuesta_oficio').show(500);
		}else{
			$('#respuesta_oficio').hide(500);
		}
	});

	$('#attachment').change(function (){
		$('#content_img').append('<div class="col-md-2 loading-img"><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div></div>');
		var form = new FormData();
		form.append("attachment", $('#attachment')[0].files[0]);
		form.append("clave", $('#clave').val());
		$.ajax({
			url: base_url+"home/imagenes",
			type: "POST",
			dataType: 'JSON',
			data: form, 
			contentType: false,
			cache: false,
			processData:false,
			success: function(response)
			{
				$('#clave').val(response.clave);
				console.log(response.status);
				if( response.status == "OK" )
				{
					console.log(response);
					$('.loading-img').remove();
					$('#content_img').append('<div class="col-md-2"><div class="box box-danger"><div class="box-tools pull-right"><button class="btn btn-danger btn-xs" type="button" data-widget="remove" data-id="'+response.imagen+'"><i class="fa fa-times"></i></button></div>'+
						'<div class="box-body"><img src="'+base_url+'upload/'+response.name+'"></div></div></div>');
				}
			}
		});
	});

	$(document).on('click', '[data-widget="remove"]', function (){
		var element = $(this),
			imagen 	= element.attr('data-id');
		element.closest('.col-md-2').remove();
		$.ajax({
			url: base_url+'home/removeImg',
			type: 'POST',
			dataType: 'JSON',
			data: {imagen: imagen},
			success: function(resp){
				console.log(resp);
			}
		});
	});

	$('#addminuta').on('click', function(e){
		$('#extras').html('<input type="hidden" name="minuta" value="t">');
		$('#form-oficio').submit();
	});
})