-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2015 a las 08:08:55
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ieepo`
--
CREATE DATABASE IF NOT EXISTS `ieepo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ieepo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

DROP TABLE IF EXISTS `acciones`;
CREATE TABLE IF NOT EXISTS `acciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `folio` int(11) NOT NULL,
  `acuerdo_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `acciones`
--

INSERT INTO `acciones` (`id`, `descripcion`, `folio`, `acuerdo_id`, `status`) VALUES
(1, 'REUNION PROGRAMADA PARA 30 DIC 13', 1, 7, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acuerdos`
--

DROP TABLE IF EXISTS `acuerdos`;
CREATE TABLE IF NOT EXISTS `acuerdos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` char(10) NOT NULL,
  `descripcion` text NOT NULL,
  `minuta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `acuerdos`
--

INSERT INTO `acuerdos` (`id`, `folio`, `descripcion`, `minuta_id`) VALUES
(1, 'I', 'LOS MAESTROS SE COMPROMETEN A LABORAR DE LUNES  A VIERNES EN HORARIO DE 9 A 14 HRS', 1),
(2, 'II', 'LOS MAESTROS SOLAMENTE FALTARAN DE SUS CENTROS EDUCATIVOS LOS DIAS 1o. DE MAYO Y 15  DE MAYO DEL 2014', 1),
(3, 'III', 'LA ASAMBLEA GENERAL ES LA QUE DETERMINARA LAS SANCIONES EN CASO DE NO CUMPLIRSE DICHOS ACUERDOS', 1),
(4, 'PRIMERO', 'EL GOBIERNO DEL ESTADO SE COMPROMETE A RESPETAR A LAS AUTORIDADES DE LA COMPAÑIA, ASI COMO A SUS USOS Y COSTUMBRES', 2),
(5, 'SEGUNDO', 'SE REALIZA EL EXHORTO A LAS AUTORIDADES DE LA COMPAÑIA PARA CONTINUAR EL DIALOGO QUE PERMITA CONSTRUIR LOS ACUERDOS QUE PERMITAN RESOLVER LAS DIFERENCIAS QUE EXISTEN EN RELACION AL TEMA EDUCATIVO', 2),
(6, 'TERCERO', 'LA AUTORIDAD MUNICIPAL DE LA COMPAÑÍA QUE INTERVIENEN EN EL ACUERDO ACEPTAN EL COMPROMISO DE CONTINUAR EL DIALOGO\r\nPERO QUE SE INVOLUCRE A LA REPRESENTACIÓN SINDICAL DE LA SECCIÓN 22', 2),
(7, 'CUARTO', 'SE ACUERDA UNA PRÓXIMA REUNIÓN PARA EL DÍA 30 DE DICIEMBRE DEL 2013', 2),
(8, 'PRIMERO', 'DOTAR A LA ESCUELA DEL SIGUIENTE MATERIAL\r\n1 LOTE ADO\r\n1 COMPUTADORA\r\n1 BANDA DE GUERRA\r\n1 IMPRESORA', 3),
(9, 'PRIMERO', 'QUE LOS DIRECTIVOS DE LOS TRES NIVELES EDUCATIVOS MANTENGAN LA COMUNICACION', 4),
(10, 'SEGUNDO', 'QUE LOS MAESTROS SEGUIRÁN LUCHANDO Y TRABAJANDO AL MISMO TIEMPO', 4),
(11, 'TERCERO', 'QUEDA BAJO CONSIDERACIÓN DE CADA MAESTRO LA DESICIÓN DE HABITAR LA CASA DEL MAESTRO', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correspondencia`
--

DROP TABLE IF EXISTS `correspondencia`;
CREATE TABLE IF NOT EXISTS `correspondencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_recepcion` date NOT NULL,
  `hora_recepcion` time NOT NULL,
  `folio` char(10) NOT NULL,
  `fecha_oficio` date NOT NULL,
  `firmado` char(250) NOT NULL,
  `descripcion` text NOT NULL,
  `tipo_documento` char(20) NOT NULL,
  `area_turnar` int(11) NOT NULL,
  `titular_area` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  `archivo` char(150) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `correspondencia`
--

INSERT INTO `correspondencia` (`id`, `fecha_recepcion`, `hora_recepcion`, `folio`, `fecha_oficio`, `firmado`, `descripcion`, `tipo_documento`, `area_turnar`, `titular_area`, `observaciones`, `archivo`, `fecha_registro`, `status`) VALUES
(1, '2013-11-11', '05:56:00', 'S/N', '2013-11-11', 'PROFR. BRAULIO C. RODRIGUEZ HERNANDEZ', 'EL QUE SUSCRIBE C. PROFR. BRAULIO C. RODRIGUEZ HERNÁNDEZ. EN CARACTER DE DIRECTOR DE LA ESCUELA Y DE ESTA INSTITUCION EDUCATIVA RESPECTIVAMENTE. POR ESTE MEDIO ME PERMITO DIRIGIRME ANTE LA OFICINA A SU CARGO CON LA FINALIDAD DE SOLICITARLE LOS SIGUIENTE MATERIALES, YA QUE CON LA PROBLEMATICA EDUCATIVA QUE SE PRESENTAN EN LA COMUNIDAD NO SE CUENTA CON DICHO MATERIAL, ADEMÁS CONTAMOS CON 65 ALUMNOS DE LOS 6 GRADOS Y NOS ENCONTRAMOS EN LUGARES PARTICULARES ENSEÑANDO A LOS ALUMNOS, POR TAL MOTIVO, ME VEO EN LA NECESIDAD DE SOLICITAR LOS SIGUIENTES MATERIALES QUE ACONTINUACION SE DESCRIBEN:', 'peticion', 3, 3, '', '', '2015-01-19 17:04:45', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

DROP TABLE IF EXISTS `estatus`;
CREATE TABLE IF NOT EXISTS `estatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Sin Respuesta', ''),
(2, 'Exp. Abierto', ''),
(3, 'Cerrado', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha_respuesta`
--

DROP TABLE IF EXISTS `fecha_respuesta`;
CREATE TABLE IF NOT EXISTS `fecha_respuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respuesta` date NOT NULL,
  `correspondencia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `fecha_respuesta`
--

INSERT INTO `fecha_respuesta` (`id`, `respuesta`, `correspondencia_id`) VALUES
(1, '2013-12-15', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'todas', 'Administrator'),
(2, 'Logistica', 'General User'),
(3, 'Administrativo', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `minuta`
--

DROP TABLE IF EXISTS `minuta`;
CREATE TABLE IF NOT EXISTS `minuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` char(25) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha` date NOT NULL,
  `lugar` char(250) NOT NULL,
  `participantes_solicitante` text NOT NULL,
  `participantes_ieepo` text NOT NULL,
  `participantes_segob` text NOT NULL,
  `participantes_seccion` text NOT NULL,
  `autoridades_municipales` text NOT NULL,
  `apoyos_entregado` text NOT NULL,
  `avance` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `minuta`
--

INSERT INTO `minuta` (`id`, `folio`, `descripcion`, `fecha`, `lugar`, `participantes_solicitante`, `participantes_ieepo`, `participantes_segob`, `participantes_seccion`, `autoridades_municipales`, `apoyos_entregado`, `avance`, `observaciones`) VALUES
(1, '0001', 'REUNION PARA TRATAR EL INICIO DE CLASES DEL CICLO ESCOLAR 2013-2014', '2013-10-20', 'SANTIAGO IXCUINTEPEC, MIXE', 'PADRES DE FAMILIA', 'LIC. RENE ARTURO ALVAREZ CUETO,SUBJEFE  DE SERVICIOS EDUCATIVOS REGION ISTMO', '.  ', 'PROFR. MIGUEL MARTINEZ LOPEZ,SECRETARIO GENERAL SINDICAL D.I.259  ', '', '', 95, 'PENDIENTES: \r\n8 BULTOS DE CEMENTO\r\n4 TAZAS PARA WC\r\n(#) MATERIAL NO AUTORIZADO POR NO CONTAR CON PARTIDA PRESUPUESTAL'),
(2, '002', 'TRATAR ASUNTOS EDUCATIVOS DEL MUNICIPIO DE LA COMPAÑIA', '2013-10-17', 'INSTALACIONES DE LA SEGEGO, CIUDAD ADMINISTRATIVA, EDIFICIO 8 PISO 2, TLALIXTAC DE CABRERA', 'AUTORIDAD MUNICIPAL', 'LIC. ADY NEZAHUALCOYOTL FLORES LOAEZA JEFE DEL DEPARTAMENTO', 'CIPRIANO EDUARDO RODRIGUEZ,DIRECTOR DE DESAROLLO POLITICO LIC. GUSTAVO GARCIA BAUTISTA ASESOR', '.', '', '', 0, ''),
(3, '003', 'ACUERDO  PARA RESOLVER LA PROBLEMÁTICA EDUCATIVA DE LA ESCUELA PRIMARIA ', '2013-11-11', 'SANTOS REYES PAPALO, CUICATLAN, OAX', 'PADRES DE FAMILIA Y DIRECTIVOS DE LA ESCUELA PRIMARIA FRANCISCO I MADERO  CLAVE : 20DPB2024G', 'LIC. EUTIQUIO FRANCO HUERTA, COORDINADOR GENERAL DE SERVICIOS REGIONALES', ' ', ' ', 'REGIDOR DE EDUCACION', 'VALE NUM. 02846 DEL 16-10-14\r\n1 COMPUTADORA\r\n1 BANDA DE GUERRA\r\n1 IMPRESORA\r\nOFICIO DE TRASLADO', 100, 'REQUERIR COPIA DE LA MINUTA DE ACUERDOS'),
(4, '004', 'TRATAR EL ASUNTO ESPECIFICO DE LA ENTREGA DE DOCUMENTOS A FIN DE DESLINDAR TODA RESPONSABILIDAD A LOS INVOLUCRADOS', '2014-12-08', 'ESPACIO DEL DEPARTAMENTO DE CERTIFICACIÓN Y CONTROL ESCOLAR DEL IEEPO', 'CC ROMAN JUAN MORALES HERNANDEZ, FAUSTINO ALMARAZ HERNANDEZ,', 'LIC. PLACIDO ARMANDO JIMÉNEZ RAMIREZ', '', 'PROFR. CRISERIO RAMIREZ ROBLES, AUXILIAR DE LA SECCION 22', '', '', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia_correspondencia`
--

DROP TABLE IF EXISTS `multimedia_correspondencia`;
CREATE TABLE IF NOT EXISTS `multimedia_correspondencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(200) NOT NULL,
  `correspondencia_id` int(11) NOT NULL,
  `clave` char(32) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` char(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `multimedia_correspondencia`
--

INSERT INTO `multimedia_correspondencia` (`id`, `archivo`, `correspondencia_id`, `clave`, `type`, `name`) VALUES
(1, 'a9a492e149638dd5b8d6a39baea7b9dc.pdf', 1, '3b2227140ee100c028e6f89abd9e8504', 'application/pdf', 'A C U E R D O S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia_minuta`
--

DROP TABLE IF EXISTS `multimedia_minuta`;
CREATE TABLE IF NOT EXISTS `multimedia_minuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(200) NOT NULL,
  `minuta_id` int(11) NOT NULL,
  `clave` char(32) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` char(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `multimedia_minuta`
--

INSERT INTO `multimedia_minuta` (`id`, `archivo`, `minuta_id`, `clave`, `type`, `name`) VALUES
(1, '6faa800550ed3b846769668bd9b43b14.pdf', 0, '74a005f3bdda4afe979ba24d33e63a86', 'application/pdf', 'A C U E R D O S'),
(2, '3fcf1e805769ed2dc1276491b147bdda.pdf', 0, '13ec95849a482aad42ffb27c6a5a6edd', 'application/pdf', 'A C U E R D O S'),
(3, '2cd5cba9aab5306b89faeb283527caf2.pdf', 0, 'e2e866d3f26487014afa8946967c7485', 'application/pdf', 'A C U E R D O S'),
(4, '9347f03d8a6c2c98d4e82636713c6fb8.pdf', 4, 'd7fdad9816c0f9411cc1c86c231a721e', 'application/pdf', 'minuta 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia_respuestas`
--

DROP TABLE IF EXISTS `multimedia_respuestas`;
CREATE TABLE IF NOT EXISTS `multimedia_respuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(200) NOT NULL,
  `respuesta_id` int(11) NOT NULL,
  `clave` char(32) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` char(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `respuesta` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$08$eg/rfv9isDjoQKHWAK6.AOzV9JzYFpLpXZ2/soVaflTenJLnEGScW', '', 'admin@admin.com', '', NULL, NULL, 'sFwPrP/ZdfFNaJdtpRXhJu', 1268889823, 1421726926, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'ricardo', '$2y$08$FZfd6fnow3cizNd8rv6vtevVyX3t79IIXqwBbpFQZYcxynkiR00nG', NULL, 'ricardo@gmail.com', NULL, NULL, NULL, NULL, 1418324789, 1418846940, 1, 'Profesor Ricardo', 'Carrasco', 'SEP', '9512134213'),
(3, '::1', 'ana', '$2y$08$SlCLzEgOqdwopKss/hAfBOI5.vtUu5K/hyoinjrIodJ9PeQzX/xFG', NULL, 'analibia@gmail.com', NULL, NULL, NULL, NULL, 1419039165, 1420763298, 1, 'Ana Libia', 'Mendoza', 'IEEPO', '123456'),
(4, '192.168.1.80', 'irma', '$2y$08$NIoW/EMC4oTmC5BXsghw.OOiWoQRymThiWcM6cN5JHplv5XLMni2G', NULL, 'IRMA@GMAIL.COM', NULL, NULL, NULL, NULL, 1421687192, 1421687192, 1, 'LIC. IRMA', 'MARTINEZ', 'IEEPO', '9511');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(25, 1, 1),
(26, 2, 2),
(23, 3, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
