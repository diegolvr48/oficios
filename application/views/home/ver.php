<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small>
        <a href="<?php echo site_url() ?>" class="btn bg-navy btn-flat pull-right"><i class="ion-ios-arrow-back"></i> Regresar</a>
        </h1>
    </section>
	<section class="content">
	    <div class="row">
	        <div class="col-sm-10 col-sm-offset-1">
				<div class="box box-solid box-ieepo">
	                <div class="box-header">
						<h3 class="box-title">Correspondencia</h3>
                        <h4 class="pull-right" style="padding-right:30px">Folio: <?php echo $datos['id'] ?> </h4>
					</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Fecha Recepción</dt>
                                    <dd><i class="ion ion-ios-calendar"></i><?php echo $datos['fecha_recepcion'] ?></dd>
                                </dl>
                            </div>
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Hora Recepción</dt>
                                    <dd><i class="ion ion-ios-clock"></i><?php echo $datos['hora_recepcion'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Numero de Oficio</dt>
                                    <dd><i class="ion ion-minus"></i><?php echo $datos['folio'] ?></dd>
                                </dl>
                            </div>
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Fecha Oficio</dt>
                                    <dd><i class="ion ion-calendar"></i><?php echo $datos['fecha_oficio'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <dl>
                                    <dt>Firmado Por</dt>
                                    <dd><i class="ion ion-person"></i><?php echo $datos['firmado'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <dl>
                                    <dt>Asunto</dt>
                                    <dd class="text-justify"><?php echo $datos['descripcion'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        <?php if(isset($files) && !empty($files)):?> 
                            <div class="row hidden-print"> 
                                <div class="col-sm-12">
                                    <h3>Archivos</h3>
                                </div>   
                                <div class="col-sm-12">
                                    <div class="row">
                                    <?php foreach($files as $file):?>
                                        <a href="<?php echo base_url('upload/'.$file['archivo']) ?>" download>
                                            <div class="col-sm-2">
                                                <div class="file">
                                                    <?php echo format_file($file['type']) ?>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Turnado a:</h3>
                            </div>
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Area</dt>
                                    <dd><i class="ion ion-ios-briefcase"></i><?php echo $group->name ?></dd>
                                </dl>
                            </div>
                            <div class="col-xs-6">
                                <dl>
                                    <dt>Titular</dt>
                                    <dd><i class="ion ion-android-contact"></i><?php echo $user->first_name.' '.$user->last_name ?></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <dl>
                                    <dt>Observaciones</dt>
                                    <dd class="text-justify"><?php echo $datos['observaciones']!=''?$datos['observaciones']:'Sin Observaciones' ?></dd>
                                </dl>
                            </div>
                        </div>

                        <?php if(isset($respuestas) && $respuestas && count($respuestas) > 0): ?>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4>Respuestas</h4>
                                </div>
                                <?php foreach( $respuestas as $respuesta): ?>
                                    <div class="col-xs-6 col-sm-4">
                                        <dl>
                                            <dt><?php echo $respuesta['titulo'] ?></dt>
                                            <dd class="text-justify"><?php echo $respuesta['respuesta'] ?></dd>
                                            <?php if( count($respuesta['files']) > 0): ?>
                                            <dd class="hidden-print"><div  class="content-files">
                                                <?php foreach($respuesta['files'] as $file): ?>
                                                <div class="file-respuesta">
                                                    <a href="<?php echo base_url('upload/'.$file['archivo']) ?>" download>
                                                        <?php echo format_file($file['type']) ?>
                                                    </a>
                                                </div>                                            
                                            <?php endforeach; ?>
                                            </div></dd>
                                        <?php endif; ?>
                                            <dd class="pull-right"><?php echo format_fecha($respuesta['fecha']) ?></dd>
                                        </dl>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="box-footer hidden-print">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <?php if( isset($minuta) && !empty($minuta) ): ?>
                                <a href="<?php echo base_url('minutas/ver/'.$minuta['minuta_id']) ?>" class="btn btn-success btn-flat">Ver minuta</a>
                                <?php endif; ?> 
                                <a href="#" class="btn btn-info btn-flat" onclick="window.print()"><i class="ion ion-ios-printer"></i> Imprimir</a>
                                <?php if($datos['status'] != 3): ?>
                                <a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#response-modal"><i class="ion ion-compose"></i> Respuesta</a>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
<div class="modal fade" id="response-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-newspaper-o"></i> Nueva Respuesta</h4>
            </div>
            <?php echo form_open_multipart(uri_string()); ?>
                <div class="modal-body">
                    <?php if( validation_errors() ):?>
                        <div class="row" id="error">
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Error!</b> <?php echo validation_errors(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <input type="hidden" name="oficio" value="<?php echo $datos['id'] ?>">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Descripcion:</span>
                            <input name="titulo" type="text" class="form-control" placeholder="Descripción">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Detalles</label>
                        <textarea name="respuesta" id="respuesta" class="form-control" placeholder="Respuesta" style="height: 120px;"></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-2 text-right">
                            <div class="btn btn-success btn-file btn-flat archivos">
                                <i class="fa fa-file-image-o"></i> 
                                <p>Archivos</p>
                                <input type="file" id="attachment"/>
                            </div>
                            <input type="hidden" name="clave" id="clave">
                            <p class="help-block">Max. 5 MB</p>
                        </div>
                        <div class="col-md-10">
                            <div class="row" id="content_img">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <h4>¿Tramite Terminado?</h4>
                        <div class="radio">
                            <label class="radio-label">
                                <input type="radio" name="terminado" class="minimal" value="no" checked/>
                                NO
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-label">
                                <input type="radio" name="terminado" class="minimal" value="si"/>
                                SI
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="respuesta_oficio">
                            <label for="" class="control-label">Fecha Respuesta:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="fecha_respuesta" name="fecha_respuesta" class="form-control datemask" data-date-format="YYYY-MM-DD"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                    <button type="submit" class="btn btn-primary pull-left btn-flat"><i class="fa fa-paper-plane"></i> Enviar</button>
                </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
<?php if( validation_errors() ):?>
    var error = true;
    <?php else: ?>
    var error;
    <?php endif; ?>
</script>