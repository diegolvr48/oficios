<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales</h1>
    </section>
	<section class="content">
	    <div class="row">
	        <div class="col-xs-12">
				<div class="box box-solid">
	                <div class="box-header">
	                	<?php if ( $is_admin ): ?>
	                		<input type="hidden" id="check">
	                	<?php endif; ?>
						<h3 class="box-title">
							Oficios
						</h3>
					</div>
					<div class="box-body table-responsive">
						<table class="table table-bordered" id="oficios">
						    <thead>
							<tr>
								<th>Folio</th>
								<th>Fecha Recepción</th>
								<th>Folio Oficio</th>
								<th>Asunto</th>
								<?php if( $is_admin ): ?>
								<th>Responsable</th>
								<?php endif; ?>
								<th>Fechas Recepcion</th>
								<th>Fechas Respuesta</th>
								<th>Estado</th>
								<th>Acciones</th>
							</tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	if ( isset($correspondencia) ): 
						    		foreach ($correspondencia as $item):
						    			$class = '';
						    			if( $item['dias_faltantes'] <= 3 && $item['dias_faltantes'] > 1 && ( $item['status'] == 1 || $item['status'] == 2 ) ) {
						    				$class = 'alert alert-warning';
						    			}elseif( $item['dias_faltantes'] <= 1 && ( $item['status'] == 1 || $item['status'] == 2 ) ){
						    				$class = 'alert alert-danger';
						    			}elseif($item['status'] == 3){
						    				$class = 'alert alert-success';
						    			}?>

						    		<tr class="<?php  echo $class ?>">
						    			<td><?php echo $item['id'] ?></td>
						    			<td><?php echo $item['fecha_recepcion'] ?></td>
						    			<td><?php echo $item['folio'] ?></td>
						    			<td><?php echo substr($item['descripcion'], 0, 60) ?></td>
						    			<?php if( $is_admin ): ?>
						    			<td><?php echo $item['nombre_titular'] ?></td>
						    			<?php endif; ?>
						    			<td><?php echo $item['fecha_oficio'] ?></td>
						    			<td><?php echo $item['respuesta'] ?></td>
						    			<td><?php echo $item['estado'] ?></td>
						    			<td>
											<div class="btn-group">
                                            	<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                                            	<ul class="dropdown-menu" role="menu">
	                                                <li><a href="<?php echo site_url('home/ver/'.$item['id']) ?>"><i class="ion ion-ios-paperplane-outline"></i> Ver</a></li>
	                                                <?php if ( $is_admin ): ?>
	                                                <li><a href="<?php echo site_url('home/editar/'.$item['id']) ?>"><i class="ion ion-edit"></i> Editar</a></li>
	                                                <li><a href="#" data-id="<?php echo $item['id'] ?>" class="eliminar"><i class="ion ion-backspace"></i> Eliminar</a></li>
                                            		<?php endif; ?>
                                            	</ul>
                                        	</div>
						    			</td>
						    		</tr>
						    	<?php endforeach;
						    	endif; ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</aside>
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" style="color:#a94442"></i> ¿Realmente quieres eliminar la correspondencia?</h4>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal"><i class="fa fa-times"></i> NO</button>
                <button type="button" class="btn btn-primary pull-left btn-flat" id="aceptar"><i class="fa fa-check"></i> SI</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script id="formulario" type="text/template">
	<form action='<?php echo base_url() ?>' method='post'>
		<button type='submit' class='btn btn-primary btn-flat pull-right' style='margin-top:-45px'>Buscar</button>
		<div class='form-group'>
		    <label for='estado'>Estado: </label>
		    <select name='estado' id='estado' class='form-control filters' col='status'>
		    	<option value=''>Todos</option>
		    	<?php if(isset($estados) && $estados): foreach ($estados as $estado): ?> 
		    		<?php if (isset($estado_ac) && $estado['id'] == $estado_ac){ $selected = 'selected';}else{$selected ='';} ?>
		    		<option value='<?php echo $estado['id'] ?>' <?php echo $selected ?>><?php echo $estado['nombre'] ?></option>
		    	<?php endforeach;endif; ?>
		    </select>
		</div>
		<hr>
		<h4 style=''>Tiempos:</h4>
		<div class='form-group'>
			<div class='radio'>
				<label><input type='radio' name='tiempos' value='' class='minimal' <?php if (!isset($tiempo_ac) || $tiempo_ac == ''){ echo 'checked';} ?>> Todas</label>
		    </div>
		    <div class='radio'>
				<label><input type='radio' name='tiempos' value='1' class='minimal' <?php if (isset($tiempo_ac) && $tiempo_ac == '1'){ echo 'checked';}?>><i class='fa fa-stop red'></i> 1 dia o menos</label>
		    </div>
		    <div class='radio'>
				<label><input type='radio' name='tiempos' value='3' class='minimal' <?php if (isset($tiempo_ac) && $tiempo_ac == '3'){ echo 'checked';}?>><i class='fa fa-stop yellow'></i> 2 ó 3 dias</label>
		    </div>
		    <div class='radio'>
				<label><input type='radio' name='tiempos' value='4' class='minimal' <?php if (isset($tiempo_ac) && $tiempo_ac == '4'){ echo 'checked';}?>><i class='fa fa-stop white'></i> +3 dias</label>
		    </div>
		</div>
	</form>
</script>