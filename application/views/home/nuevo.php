<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small></h1>
    </section>
	<section class="content">
        <?php if( validation_errors() ): ?>
        <div class="row" id="errores">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Error!</b> <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
				<div class="box box-success">
	                <div class="box-header">
						<h3 class="box-title">Correspondencia</h3>
					</div>
					<?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal','id' => 'form-oficio'));?>
					<div class="box-body">
                        <div class="form-group col-sm-6">
                            <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha Recepción</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="fecha_recepcion" name="fecha_recepcion" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo set_value('fecha_recepcion', date('Y-m-d')) ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-sm-4 control-label">Hora Recepción:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" class="form-control timepicker" name="hora_recepcion" value="<?php echo set_value('hora_recepcion', date('H:i'))?>"/>
                                </div>
                            </div>
                        </div>
						<div class="form-group col-sm-6">
                            <label for="folio" class="col-sm-4 control-label">No. Oficio</label>
                            <div class="col-sm-8"><input type="text" class="form-control" id="folio" name="folio" placeholder="Numero de Oficio" value="<?php echo set_value('folio')?>"></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha Oficio</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="fecha_oficio" name="fecha_oficio" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo set_value('fecha_oficio')?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Firmado Por:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" id="firmado" name="firmado" placeholder="Firmado Por" value="<?php echo set_value('firmado')?>"></div>
                        </div>
			            <div class="form-group">
                            <label for="descripcion" class="col-sm-2 control-label">Asunto: </label>
                            <div class="col-sm-10"><textarea name="descripcion" id="descripcion" class="form-control"><?php echo set_value('descripcion')?></textarea></div>
                        </div>
                        <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="" class="col-sm-4 control-label">Tipo Documento:</label>
                            <div class="col-sm-8">
                                <select name="tipo_documento" id="tipo_documento" class="form-control">
                                    <option value="peticion" <?php echo set_select('tipo_documento', 'peticion', TRUE); ?>>Oficio</option>
                                    <option value="informativo" <?php echo set_select('tipo_documento', 'informativo'); ?>>Informativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div id="respuesta_oficio">
                                <label for="" class="col-sm-4 control-label">Fecha Respuesta:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="fecha_respuesta" name="fecha_respuesta" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo set_value('fecha_respuesta')?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-2 text-right">
                                <div class="btn btn-success btn-file btn-flat archivos">
                                    <i class="fa fa-file-image-o"></i> 
                                    <p>Archivos</p>
                                    <input type="file" id="attachment"/>
                                </div>
                                <input type="hidden" name="clave" id="clave" value="<?php echo set_value('clave')?>">
                                <p class="help-block">Max. 5 MB</p>
                            </div>
                            <div class="col-md-10">
                                <div class="row" id="content_img">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p class="lead">Turnar Oficio para Atención</p>
                        <div class="form-group col-sm-5">
                        	<label for="" class="col-sm-5">Area: </label>
                        	<div class="col-sm-7">
                        		<select name="area_turnar" id="area_turnar" class="form-control">
                        			<option value=""></option>
									<?php foreach ($groups as $group):?>
										<option value="<?php echo $group->id ?>"><?php echo $group->name;?></option>
									<?php endforeach;?>
                        		</select>
                                <?php if( $minuta = $this->session->flashdata('minuta') ): ?>
                                    <input type="hidden" name="folioMinuta" value="<?php echo $minuta ?>">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group col-sm-7">
                            <label for="" class="col-sm-3">Titular</label>
                            <div class="col-sm-9">
                                <select name="titular_area" id="titular_area" class="form-control">
                                </select>
                            </div>
					    </div>
                        <div class="form-group">
                            <label for="observaciones" class="col-sm-2 control-label">Observaciones: </label>
                            <div class="col-sm-10"><textarea name="observaciones" id="observaciones" class="form-control"><?php echo set_value('observaciones') ?></textarea></div>
                        </div>
                        <div class="hidden" id="extras">
                        </div>
					<div class="box-footer text-center">
                        <a href="<?php echo site_url() ?>" class="btn btn-danger btn-flat"><i class="fa fa-times"></i> Cancelar</a>
						<button type="submit" class="btn btn-primary btn-large"> Agregar</button>
                        <?php if( !isset($minuta) || !$minuta ): ?>
                            <button type="button" id="addminuta" class="btn btn-warning btn-large"> Agregar Minuta</button>
                        <?php endif; ?>
					</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</section>
</aside>