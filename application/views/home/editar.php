<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small></h1>
    </section>
	<section class="content">
        <?php if( validation_errors() ): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Error!</b> <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
				<div class="box box-success">
	                <div class="box-header">
						<h3 class="box-title">Correspondencia</h3>
					</div>
					<?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal'));?>
					<div class="box-body">
                        <div class="form-group col-sm-6">
                            <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha Recepción</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="fecha_recepcion" name="fecha_recepcion" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo $datos['fecha_recepcion'] ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-sm-4 control-label">Hora Recepción:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" class="form-control timepicker" name="hora_recepcion" value="<?php echo $datos['hora_recepcion'] ?>"/>
                                </div>
                            </div>
                        </div>
						<div class="form-group col-sm-6">
                            <label for="folio" class="col-sm-4 control-label">No. Oficio</label>
                            <div class="col-sm-8"><input type="text" class="form-control" id="folio" name="folio" placeholder="Numero de Oficio" value="<?php echo $datos['folio'] ?>"></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha Oficio</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="fecha_oficio" name="fecha_oficio" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo $datos['fecha_oficio'] ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Firmado Por:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" id="firmado" name="firmado" placeholder="Firmado Por" value="<?php echo $datos['firmado'] ?>"></div>
                        </div>
			            <div class="form-group">
                            <label for="descripcion" class="col-sm-2 control-label">Asunto: </label>
                            <div class="col-sm-10"><textarea name="descripcion" id="descripcion" class="form-control"><?php echo $datos['descripcion'] ?></textarea></div>
                        </div>
                        <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="" class="col-sm-4 control-label">Tipo Documento:</label>
                            <div class="col-sm-8">
                                <select name="tipo_documento" id="tipo_documento" class="form-control">
                                    <?php switch ($datos['tipo_documento']) {
                                        case 'peticion':
                                            $peticion = 'selected';
                                            break;
                                        case 'informativo':
                                            $informativo = 'selected';
                                            break;
                                        default:
                                            $default = 'selected';
                                            break;
                                    } ?>
                                    <option value="" <?php echo isset($default)?$default:''; ?>></option>
                                    <option value="peticion" <?php echo isset($peticion)?$peticion:''; ?>>Oficio</option>
                                    <option value="informativo" <?php echo isset($informativo)?$informativo:''; ?>>Informativo</option>
                                </select>
                                <input type="hidden" name="id_correspondencia" value="<?php echo $datos['id'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div id="respuesta_oficio" <?php if( !isset($peticion)): ?> style="display:none;" <?php endif; ?>>
                                <label for="" class="col-sm-4 control-label">Fecha Respuesta:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="fecha_respuesta" name="fecha_respuesta" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo $respuesta['respuesta']?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-2 text-right">
                                <div class="btn btn-success btn-file btn-flat archivos">
                                    <i class="fa fa-file-image-o"></i> 
                                    <p>Archivos</p>
                                    <input type="file" id="attachment"/>
                                </div>
                                <input type="hidden" name="clave" id="clave" <?php if(isset($files[0])): ?> value="<?php echo $files[0]['clave'] ?>" <?php endif; ?>>
                                <p class="help-block">Max. 5 MB</p>
                            </div>
                            <div class="col-md-10">
                                <div class="row" id="content_img">
                                <?php if( isset($files)):
                                foreach($files as $file): ?>
                                    <div class="col-md-2">
                                        <div class="box box-danger">
                                            <div class="box-tools pull-right">
                                                <button class="btn btn-danger btn-xs" type="button" data-widget="remove" data-id="<?php echo $file['id'] ?>"><i class="fa fa-times"></i></button>
                                            </div>
                                            <div class="box-body">
                                                <?php switch ($file['type']) {
                                                    case 'application/pdf':
                                                        $file['archivo'] = 'pdf_icon.jpg';
                                                        break;
                                                } ?>
                                                <img src="<?php echo base_url('upload/'.$file['archivo']) ?>">
                                            </div>
                                            <div class="box-footer text-center">
                                                <?php echo $file['name'] ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php  endforeach;
                                endif;?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p class="lead">Turnar Oficio para Atención</p>
                        <div class="form-group col-sm-5">
                        	<label for="" class="col-sm-5">Area: </label>
                        	<div class="col-sm-7">
                        		<select name="area_turnar" id="area_turnar" class="form-control">
                        			<option value=""></option>
									<?php foreach ($groups as $group):
                                        if ( $datos['area_turnar'] == $group->id):
                                            $selected = 'selected';
                                        else:
                                            $selected = '';
                                        endif;
                                    ?>

										<option value="<?php echo $group->id ?>" <?php echo $selected ?>><?php echo $group->name;?></option>
									<?php endforeach;?>
                        		</select>
                            </div>
                        </div>
                        <div class="form-group col-sm-7">
                            <label for="" class="col-sm-3">Titular</label>
                            <div class="col-sm-9">
                                <select name="titular_area" id="titular_area" class="form-control">
                                    <?php foreach($users as $user):
                                        if( $datos['titular_area'] == $user->id):
                                            $selected = 'selected';
                                        else:
                                            $selected = '';
                                        endif; ?>
                                        <option value="<?php echo $user->id ?>" <?php echo $selected ?>><?php echo $user->first_name.' '.$user->last_name;?></option>
                                        <?php endforeach;?>
                                </select>
                            </div>
					    </div>
                        <div class="form-group">
                            <label for="observaciones" class="col-sm-2 control-label">Observaciones: </label>
                            <div class="col-sm-10"><textarea name="observaciones" id="observaciones" class="form-control"><?php echo $datos['observaciones'] ?></textarea></div>
                        </div>
					<div class="box-footer text-center">
                        <a href="<?php echo site_url() ?>" class="btn btn-danger btn-flat"><i class="fa fa-times"></i> Cancelar</a>
						<button type="submit" class="btn btn-primary btn-large"> Guardar</button>
					</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</section>
</aside>