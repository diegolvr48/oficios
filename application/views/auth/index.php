<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo lang('index_subheading');?>
        	<small></small></h1>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if(!empty($message)) echo '<div id="infoMessage" class="alert alert-warning"><i class="fa fa-warning"></i> '.$message.'</div>';?>
			</div>
			<div class="col-xs-12 text-right">
				<p><?php echo anchor('auth/create_user', lang('index_create_user_link'), array('class' => 'btn btn-success')); ?>
 					&nbsp; <?php echo anchor('auth/create_group', lang('index_create_group_link'), array('class' => 'btn btn-primary')); ?></p>
			</div>
		</div>
	    <div class="row">
	        <div class="col-xs-12">
				<div class="box">
	                <div class="box-header">
						<h3>
							Usuarios
						</h3>
					</div>
					<div class="box-body table-responsive">
						<table class="table table-bordered table-striped table-hover" id="usuarios">
						    <thead>
							<tr>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
						    </thead>
						    <tbody>
							<?php foreach ($users as $user):?>
								<tr>
									<td><?php echo $user->first_name;?></td>
									<td><?php echo $user->last_name;?></td>
									<td><?php echo $user->email;?></td>
									<td>
										<?php foreach ($user->groups as $group):?>
											<?php echo anchor("auth/edit_group/".$group->id, $group->name) ;?><br />
						                <?php endforeach?>
									</td>
									<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link'), array('class' => 'btn btn-danger')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'), array('class' => 'btn btn-danger'));?></td>
									<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit', array('class' => 'btn btn-primary')) ;?></td>
								</tr>
							<?php endforeach;?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</aside>