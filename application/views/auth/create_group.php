<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Crear Area
          <small></small><p class="pull-right"><a class="btn btn-warning" href="<?php echo base_url('auth'); ?>">Atras</a></p></h1>
    </section>
  <section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if(!empty($message)) echo '<div id="infoMessage" class="alert alert-warning"><i class="fa fa-warning"></i> '.$message.'</div>';?>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?php echo lang('create_group_subheading'); ?></h3>
                </div>

                <?php echo form_open("auth/create_group", array('class' => 'form-horizontal')); ?>
                <div class="box-body">
                    <div class="form-group">
                        <?php echo form_label($this->lang->line('create_group_name_label'), 'group_name', array('class' => 'col-sm-3 control-label')); ?>
                        <div class="col-sm-5"><?php echo form_input($group_name, NULL, 'class="form-control"'); ?></div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label($this->lang->line('create_group_desc_label'), 'description', array('class' => 'col-sm-3 control-label')); ?>
                        <div class="col-sm-5"><?php echo form_input($description, NULL, 'class="form-control"'); ?></div>
                    </div>
                </div>
                <div class="box-footer text-center">
                    <?php echo form_submit(array('name' => 'submit', 'class' => 'btn btn-primary btn-large', 'value' => 'Create')); ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
  </section>
</aside>