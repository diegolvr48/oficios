<style>
    body{
        background-image: url(../assets/img/Bottom_texture.jpg);
    }
    .header-login{
        background-image: url(../assets/img/ieepo_header.png);
        background-repeat: no-repeat;
        background-position: center;
        height: 90px;
    }
</style>
<div class="header-login">
    
</div>
        <div class="form-box" id="login-box">
            <div class="header">Iniciar Sesión</div>
            <?php echo form_open("auth/login"); ?>
                <div class="body bg-gray">
                    <div class="form-group">
                        <?php echo form_input($identity); ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_input($password); ?>
                    </div>          
                    <div class="form-group">
                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?> <?php echo $this->lang->line('login_remember_label'); ?>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-login btn-block btn-flat">Entrar</button>  
                </div>
            <?php echo form_close(); ?>
        </div>
    <?php if(!empty($message)) echo '<div id="infoMessage" class="alert alert-warning">'.$message.'</div>';?>