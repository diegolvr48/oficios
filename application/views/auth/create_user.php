<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Crear Usuario
          <small></small><p class="pull-right"><a class="btn btn-warning" href="<?php echo base_url('auth'); ?>">Atras</a></p></h1>
    </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
<?php if(!empty($message)) echo '<div id="infoMessage" class="alert alert-warning"><i class="fa fa-warning"></i> '.$message.'</div>';?>
      </div>
      <div class="col-md-10 col-md-offset-1">
      <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title"><?php echo lang('create_user_subheading');?></h3>
          </div>
            <?php echo form_open("auth/create_user", array('class' => 'form-horizontal'));?>
            <div class="box-body">
            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_fname_label'), 'first_name', array('class' => 'col-sm-3 control-label'));?> 
                <div class="col-sm-5"><?php echo form_input($first_name, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_lname_label'), 'first_name', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($last_name, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_company_label'), 'company', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($company, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_email_label'), 'email', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($email, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_phone_label'), 'phone', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($phone, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_password_label'), 'password', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($password, NULL, 'class="form-control"');?></div>
            </div>

            <div class="form-group">
                  <?php echo form_label($this->lang->line('create_user_password_confirm_label'), 'password_confirm', array('class' => 'col-sm-3 control-label'));?> 
                  <div class="col-sm-5"><?php echo form_input($password_confirm, NULL, 'class="form-control"');?></div>
            </div>
      </div>

      <div class="box-footer text-center">
          <?php echo form_submit(array('name' => 'submit', 'value' => 'Agregar', 'class' => 'btn btn-primary btn-large'));?>
      </div>

<?php echo form_close();?>
</div>
      </div>
    </div>
  </section>
</aside>