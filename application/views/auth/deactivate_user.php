<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Desactivar Usuario
          <small></small><p class="pull-right"><a class="btn btn-warning" href="<?php echo base_url('auth'); ?>">Atras</a></p></h1>
    </section>
  <section class="content">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title"><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></h3>
          </div>
<?php echo form_open("auth/deactivate/".$user->id, array('class' => 'form-horizontal'));?>
          <div class="box-body">

          <div class="form-group"><label>
        	 <input type="radio" name="confirm" value="yes" checked="checked"> <?php echo $this->lang->line('deactivate_confirm_y_label');?>
          </label></div>
        <div class="form-group"><label>
          <input type="radio" name="confirm" value="no"> <?php echo $this->lang->line('deactivate_confirm_n_label'); ?>
        </label></div>
  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>
          </div>
  <div class="box-footer text-center">
          <?php echo form_submit(array('name' => 'submit', 'value' => 'Deactivate', 'class' => 'btn btn-danger'));?>
  </div>
<?php echo form_close();?>
</div>
      </div>
    </div>
  </section>
</aside>