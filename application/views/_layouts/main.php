<?php $this->load->view('_blocks/header'); ?>
<?php if(isset($logged_in) && $logged_in) $this->load->view('_blocks/navbar'); ?>
<?php $this->load->view($view_file); ?>
<?php $this->load->view('_blocks/footer'); ?>