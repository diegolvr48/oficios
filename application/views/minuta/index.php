<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales</h1>
    </section>
	<section class="content">
	    <div class="row">
	        <div class="col-xs-12">
				<div class="box box-solid">
	                <div class="box-header">
	                	<?php if ( $is_admin ): ?>
	                		<input type="hidden" id="check">
	                	<?php endif; ?>
						<h3 class="box-title">
							Minutas de Acuerdos
						</h3>
					</div>
					<div class="box-body table-responsive">
						<table class="table table-bordered" id="oficios">
						    <thead>
							<tr>
								<th>Folio</th>
								<th>Fecha</th>
								<th>Descripción</th>
								<th>Avance</th>
								<th>Acciones</th>
							</tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	if ( isset($minutas) ): 
						    		foreach ($minutas as $item):?>
						    		<tr <?php echo minuta_progress($item['avance']) ?>>
						    			<td><?php echo $item['folio'] ?></td>
						    			<td><?php echo $item['fecha'] ?></td>
						    			<td><?php echo substr($item['descripcion'],0,100) ?></td>
						    			<td>
						    				<?php $progress = $item['avance']; ?>
						    				<div class="progress" data-toggle="tooltip" data-placement="top" title="<?php echo $progress ?>%">
                                        		<div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress ?>%">
                                            		<span class="sr-only"><?php echo $progress ?>%</span>
                                        		</div>
                                    		</div>
						    			</td>
						    			<td>
											<div class="btn-group">
                                            	<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                                            	<ul class="dropdown-menu" role="menu">
	                                                <li><a href="<?php echo site_url('minutas/ver/'.$item['id']) ?>"><i class="ion ion-ios-paperplane-outline"></i> Ver</a></li>
	                                                <?php if ( $is_admin ): ?>
	                                                <li><a href="<?php echo site_url('minutas/editar/'.$item['id']) ?>"><i class="ion ion-edit"></i> Editar</a></li>
	                                                <li><a href="#" data-id="<?php echo $item['id'] ?>" class="eliminar"><i class="ion ion-backspace"></i> Eliminar</a></li>
                                            		<?php endif; ?>
                                            	</ul>
                                        	</div>
						    			</td>
						    		</tr>
						    	<?php endforeach;
						    	endif; ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</aside>
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" style="color:#a94442"></i> ¿Realmente quieres eliminar la correspondencia?</h4>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal"><i class="fa fa-times"></i> NO</button>
                <button type="button" class="btn btn-primary pull-left btn-flat" id="aceptar"><i class="fa fa-check"></i> SI</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->