<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small>
        <a href="<?php echo site_url('minutas') ?>" class="btn bg-navy btn-flat pull-right"><i class="ion-ios-arrow-back"></i> Regresar</a>
        </h1>
    </section>
	<section class="content">
	    <div class="row">
	        <div class="col-sm-10 col-sm-offset-1">
                <div class="row visible-print-block">
                    <div class="col-xs-2">
                        <img src="<?php echo base_url('assets/img/LOGOS.png') ?>"/>
                    </div>
                    <div class="col-xs-10 text-center">
                        <h5>INSTITUTO ESTATAL DE EDUCACIÓN PÚBLICA DE OAXACA</h5>
                       <h5> Coordinación General de Servicios Regionales y Descentralización Educativa </h5>
                    </div>
                </div>
				<div class="box box-solid box-ieepo">
	                <div class="box-header">
						<h3 class="box-title text-center">Control de Minutas</h3>
                        <h4 class="pull-right" style="padding-right:30px">Folio: <?php echo $minuta['folio'] ?> </h4>
					</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6 col-xs-3">
                                <dl>
                                    <dt class="float-print">Fecha: </dt>
                                    <dd class="float-print margin-print"><i class="ion ion-ios-calendar hidden-print"></i><?php echo $minuta['fecha'] ?></dd>
                                </dl>
                            </div>
                            <div class="col-xs-9 col-sm-6">
                                <dl>
                                    <dt class="float-print">Lugar: </dt>
                                    <dd class="text-justify float-print margin-print" ><i class="ion ion-ios-location hidden-print"></i><?php echo $minuta['lugar'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        <hr class="linea-print">
                        <div class="row">
                            <div class="col-xs-12">
                                <dl>
                                    <h3>Asunto General</h3>
                                    <dd class="text-justify"><?php echo $minuta['descripcion'] ?></dd>
                                </dl>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Participantes:</h3>
                            </div>
                            <?php if( !empty($minuta['participantes_ieepo'])): ?>
                            <div class="col-xs-12">
                                <dl>
                                    <dt class="float-print">IEEPO:</dt>
                                    <dd class="text-justify float-print margin-print"><?php echo $minuta['participantes_ieepo'] ?></dd>
                                </dl>
                            </div>
                        <?php endif;
                                if( !empty($minuta['participantes_segob'])): ?>
                            <div class="col-xs-12">
                                <dl>
                                    <dt class="float-print">SEGOB:</dt>
                                    <dd class="text-justify float-print margin-print"><?php echo $minuta['participantes_segob'] ?></dd>
                                </dl>
                            </div>
                        <?php endif;
                            if( !empty($minuta['participantes_seccion'])): ?>
                            <div class="col-xs-12">
                                <dl>
                                    <dt class="float-print">SECCION XXII:</dt>
                                    <dd class="text-justify float-print margin-print"><?php echo $minuta['participantes_seccion'] ?></dd>
                                </dl>
                            </div>
                        <?php endif; 
                                if( !empty($minuta['autoridades_municipales'])):?>
                            <div class="col-xs-12">
                                <dl>
                                    <dt class="float-print">AUTORIDADES MUNICIPALES:</dt>
                                    <dd class="text-justify float-print margin-print min-width"><?php echo $minuta['autoridades_municipales'] ?></dd>
                                </dl>
                            </div>
                        <?php endif; ?>
                            <div class="col-xs-12">
                                <dl>
                                    <dt>OTROS PARTICIPANTES:</dt>
                                    <dd><?php echo $minuta['participantes_solicitante'] ?></dd>
                                </dl>
                            </div>
                        </div>
                         <?php if(isset($files) && !empty($files)):?> 
                            <div class="row hidden-print"> 
                                <div class="col-sm-12">
                                    <h3>Archivos</h3>
                                </div>   
                                <div class="col-sm-12">
                                    <div class="row">
                                    <?php foreach($files as $file):?>
                                        <a href="<?php echo base_url('upload/'.$file['archivo']) ?>" download>
                                            <div class="col-sm-2">
                                                <div class="file">
                                                    <?php echo format_file($file['type']) ?>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 style="margin:0 0 20px;">Acuerdos
                                <a href="<?php echo site_url('minutas/nuevo_acuerdo/'.$minuta['id']) ?>" class="btn btn-flat btn-warning pull-right hidden-print"> Nuevo</a>
                                </h3>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-bordered table-striped" id="acuerdos">
                                    <thead>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Descripción</th>
                                        <th class="hidden-print">Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if ( isset($acuerdos) ): 
                                            foreach ($acuerdos as $item):?>
                                            <tr row="<?php echo $item['id'] ?>" data-minuta="<?php echo $item['minuta_id'] ?>">
                                                <td><span><?php echo $item['folio'] ?></span></td>
                                                <td><?php echo $item['descripcion'] ?></td>
                                                <td class="hidden-print" width="11%">
                                                    <div class="btn-group">
                                                        <a href="<?php echo site_url('minutas/nuevo_acuerdo/'.$item['minuta_id'].'/'.$item['id']) ?>" class="btn btn-success btn-flat btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><i class="ion ion-edit"></i></a>
                                                        <a class="btn btn-danger btn-flat btn-sm eliminar" data-id="<?php echo $item['id'] ?>" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="ion ion-trash-b"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                        endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 style="margin:0 0 20px;">Avances
                                <a id="apoyo" class="btn btn-flat btn-warning hidden-print pull-right"> Ingresar Apoyo</a>
                                </h3>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <th width="87" class="text-center">
                                                PORCENTAJE
                                            </th>
                                            <?php if( !empty($minuta['apoyos_entregado'])): ?>
                                            <th width="30%">
                                                APOYO ENTREGADO
                                            </th>
                                            <?php endif; ?>
                                            <?php if( !empty($minuta['observaciones'])): ?>
                                            <th width="40%">
                                                OBSERVACIONES
                                            </th>
                                            <?php endif; ?>
                                            <?php if( !empty($oficios) ): ?>
                                            <th width="20%" class="text-center hidden-print">
                                                OFICIO
                                            </th>
                                            <?php endif; ?>
                                        </tr>
                                        <tr>
                                            <td width="20%" class="text-center">
                                                <?php echo $minuta['avance'] ?>%
                                            </td>
                                            <?php if( !empty($minuta['apoyos_entregado'])): ?>
                                            <td width="40%">
                                                <?php echo $minuta['apoyos_entregado'] ?>
                                            </td>
                                            <?php endif; ?>
                                            <?php if( !empty($minuta['observaciones'])): ?>
                                            <td width="40%">
                                                <?php echo $minuta['observaciones'] ?>
                                            </td>
                                            <?php endif; ?>
                                            <?php if( !empty($oficios)): ?>
                                            <td width="20%" class="text-center hidden-print">
                                                <a href="<?php echo base_url('home/ver/'.$oficios[0]['oficio_id']) ?>" class="btn btn-success btn-flat">Ver Oficio</a>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
<div class="modal fade" id="modal-apoyo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-tasks"></i> Apoyos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo form_open(uri_string()); ?>
                         <div class="form-group">
                            <label for="entregados">Apoyos Entregados</label>
                           <textarea class="form-control" name="entregado" row="3"><?php echo $minuta['apoyos_entregado'] ?></textarea>
                          </div>
                          <div class="form-group">
                            <label for="avance">Avance</label>
                            <input type="number" class="form-control" id="avance" name="avance" value="<?php echo $minuta['avance'] ?>">
                          </div>
                          <div class="form-group">
                          <label for="entregados">Observaciones</label>
                            <input type="hidden" name="minuta" value="<?php echo $minuta['id'] ?>">
                            <textarea class="form-control" name="observaciones" row="3"><?php echo $minuta['observaciones'] ?></textarea>
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" class="btn btn-danger btn-flat pull-left">Cancelar</a>
                <button type="submit" class="btn btn-info">Aceptar</button>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>