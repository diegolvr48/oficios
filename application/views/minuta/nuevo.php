<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small></h1>
    </section>
	<section class="content">
        <?php if( validation_errors() ): ?>
        <div class="row" id="errores">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Error!</b> <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
				<div class="box box-success">
	                <div class="box-header">
						<h3 class="box-title">Minuta de Acuerdos</h3>
					</div>
					<?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-minuta'));?>
					<div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="fecha" name="fecha" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo set_value('fecha', date('Y-m-d')) ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="folio" class="col-sm-4 control-label">Folio</label>
                                <div class="col-sm-8">
                                    <input type="text" id="folio" name="folio" class="form-control" value="<?php echo set_value('folio') ?>"/><input type="hidden" value="<?php echo set_value('acuerdos', 'f') ?>" name="acuerdos">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lugar" class="col-sm-2 control-label">Lugar</label>
                            <div class="col-sm-10">
                                <input type="text" id="lugar" name="lugar" class="form-control" value="<?php echo set_value('lugar') ?>"/>
                            </div>
                        </div>
                        <h3>Participantes</h3>
                        <hr>                    
                        <div class="form-group">
                            <label for="solicitantes" class="col-sm-2 control-label">Participantes</label>
                            <div class="col-sm-10">
                                <textarea name="solicitantes" id="solicitantes" rows="2" class="form-control"><?php echo set_value('solicitantes') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ieepo" class="col-sm-2 control-label">IEEPO</label>
                            <div class="col-sm-10">
                                <textarea name="ieepo" id="ieepo" class="form-control" rows="2"><?php echo set_value('ieepo') ?></textarea>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="segob" class="col-sm-2 control-label">SEGOB</label>
                            <div class="col-sm-10">
                                <textarea name="segob" id="segob" class="form-control" rows="2"><?php echo set_value('segob') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="segob" class="col-sm-2 control-label">Autoridad Municipal</label>
                            <div class="col-sm-10">
                                <textarea name="municipal" id="municipal" class="form-control" rows="2"><?php echo set_value('municipal') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="seccion" class="col-sm-2 control-label">Seccion XXII</label>
                            <div class="col-sm-10">
                                <textarea name="seccion" id="seccion" class="form-control" rows="2"><?php echo set_value('seccion') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="descripcion" class="col-sm-2 control-label">Problematica</label>
                            <div class="col-sm-10">
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="3"><?php echo set_value('descripcion') ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="isEscuela" class="col-sm-2 control-label">¿Es escuela?</label>
                            <div class="col-sm-4">
                                <label for=""><input type="radio" name="isEscuela" value="SI" <?php echo set_checkbox('isEscuela', 'SI'); ?>>SI</label>
                                <label for=""><input type="radio" name="isEscuela" value="NO" <?php echo set_checkbox('isEscuela', 'NO', TRUE); ?>>NO</label>
                            </div>
                            <div class="escuela" <?php echo set_value('isEscuela')=='SI'?'':'style="display:none"' ?> >
                                <label for="claveEscuela" class="col-sm-2 control-label">Clave Escuela</label>
                                <div class="col-sm-4">
                                    <input type="text" name="claveEscuela" id="claveEscuela" class="form-control" value="<?php echo set_value('claveEscuela') ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group escuela" <?php echo set_value('isEscuela')=='SI'?'':'style="display:none"' ?> >
                            <label for="nombreEscuela" class="col-sm-2 control-label">Nombre Escuela</label>
                            <div class="col-sm-10">
                                <input name="nombreEscuela" id="nombreEscuela" class="form-control" value="<?php echo set_value('nombreEscuela') ?>">
                            </div>
                        </div>
                        <?php if( $oficio = $this->session->flashdata('oficio') ): ?>
                        <div class="hidden">
                            <input type="hidden" name="oficio" value="<?php echo set_value('oficio', $oficio) ?>">
                        </div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="form-group col-md-2 text-right">
                                <div class="btn btn-success btn-file btn-flat archivos">
                                    <i class="fa fa-file-image-o"></i> 
                                    <p>Archivos</p>
                                    <input type="file" id="attachment"/>
                                </div>
                                <input type="hidden" name="clave" id="clave" value="<?php echo set_value('clave') ?>">
                                <p class="help-block">Max. 5 MB</p>
                            </div>
                            <div class="col-md-10">
                                <div class="row" id="content_img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-center">                        
                        <button type="submit" class="btn btn-primary btn-flat"> Guardar</button>
                        <a class="btn btn-success btn-flat" id="acuerdos"> Agregar Acuerdos</a>
                        <a href="<?php echo site_url('minutas') ?>" class="btn btn-danger btn-flat"><i class="fa fa-times"></i> Cancelar</a>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</aside>
