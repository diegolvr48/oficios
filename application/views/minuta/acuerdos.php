<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
            <a href="<?php echo site_url('minutas/ver/'.$minuta) ?>" class="btn bg-navy btn-flat pull-right"><i class="ion-ios-arrow-back"></i> Atras</a>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <?php if( validation_errors() ): ?>
                <div class="row" id="errores">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Error!</b> <?php echo validation_errors(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Acuerdos</h3>
                    </div>
                    <?php echo form_open(uri_string(), array('class' => 'form-horizontal','id' => 'form-acuerdo'));?>
                    <div class="box-body">
                        <div class="hidden" id="folio-acuerdo">
                            <?php if ( isset($acuerdo_data) && !empty($acuerdo_data)): ?>
                                <input type="hidden" name="folio_acuerdo" value="<?php echo $acuerdo_data['id'] ?>">
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label for="folio" class="col-sm-3 control-label">Numero</label>
                            <input type="hidden" name="minuta" value="<?php echo $minuta ?>">
                            <div class="col-sm-9">
                                <input type="hidden" name="terminado" value="f">
                                <input type="text" id="folio" name="folio" class="form-control" <?php if ( isset($acuerdo_data) && !empty($acuerdo_data)){ echo 'value ="'.$acuerdo_data['folio'].'"';}else{echo 'value="I"';} ?> />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="descripcion" class="col-sm-3 control-label">Descripción</label>
                            <div class="col-sm-9">
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="10"><?php if ( isset($acuerdo_data) && !empty($acuerdo_data)){ echo $acuerdo_data['descripcion'];}?></textarea>
                            </div>
                        </div>
                        <div class="form-group hidden" id="extras"></div>
                    </div>
                    <div class="box-footer text-center">                        
                        <button type="submit" class="btn btn-primary btn-flat"> Guardar</button>
                        <button type="button" class="btn btn-flat btn-success" id="terminar">Guardar y Terminar</button>
                        <button type="button" class="btn btn-flat btn-warning" id="addoficio">Agregar Oficio</button>
                        <a href="<?php echo site_url('minutas') ?>" class="btn btn-danger btn-flat"><i class="fa fa-times"></i> Cancelar</a>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Acuerdos</h3>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th>Folio</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </tr>
                            <?php if(isset($acuerdos_list)): 
                            foreach( $acuerdos_list as $acuerdo ):?>
                            <tr>
                                <td><span><?php echo $acuerdo['folio'] ?></span></td>
                                <td><?php echo $acuerdo['descripcion'] ?></td>
                                <td width="20%">
                                    <div class="btn-group">
                                    <a class="btn btn-info btn-sm btn-flat more-acciones" data-id="<?php echo $acuerdo['id'] ?>" data-toggle="tooltip" data-placement="top" title="Agregar Acciones"><i class="fa fa-plus"></i></a>
                                    <a class="btn btn-success btn-sm btn-flat edit-acuerdo" data-id="<?php echo $acuerdo['id'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger btn-sm btn-flat delete-acuerdo" data-id="<?php echo $acuerdo['id'] ?>" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times "></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach;endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
<script>
    <?php if(isset($acuerdo_id)): ?>
    var ACUERDOG = <?php echo $acuerdo_id ?>;
    <?php else: ?>
    var ACUERDOG;
    <?php endif; ?>
</script>
<div class="modal fade" id="modal-acciones" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-tasks"></i> Acciones</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="pregunta">
                    <div class="col-sm-12">
                        <h4>¿Desea agregar Acciones al acuerdo?</h4>
                        <div class="radio">
                            <label for="" class="check"><input type="radio" name="check-acciones" value="si"> SI</label>
                        </div>
                        <div class="radio">
                            <label for="" class="check"><input type="radio" name="check-acciones" value="no"> NO</label>
                        </div>
                    </div>
                </div>
                <div class="row formulario" style="display:none">
                    <div class="form-horizontal">
                        <div class="hidden" id="folio-accion"></div>
                        <div class="form-group">
                            <label for="folio_acuerdo" class="col-sm-3 control-label">Folio Acuerdo</label>
                            <div class="col-sm-9">
                                <input type="text" id="folio_acuerdo" class="form-control" value="<?php echo isset($folio_acuerdo)?$folio_acuerdo:'' ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="accion-nueva" class="col-sm-3 control-label">Descripcion Acción</label>
                            <div class="col-sm-9">
                                <input type="text" id="descripcion-accion" class="form-control">
                                <input type="hidden" id="acuerdo" value="<?php echo isset($acuerdo_id)?$acuerdo_id:'' ?>">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary btn-flat formulario" type="button" id="guardar-accion">Guardar</button>
                            <button type="button" class="btn btn-flat btn-success" id="terminar_acciones">Guardar y Terminar</button>
                            <button class="btn btn-danger btn-flat" type="button" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
                <div class="row formulario" style="display:none">
                    <div class="box box-success">
                        <div class="box-body no-padding">
                            <table class="table table-bordered" id="acciones">
                                <thead>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Descripción</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="aceptar-accion">Aceptar</button>
            </div>
        </div>
    </div>
</div>
