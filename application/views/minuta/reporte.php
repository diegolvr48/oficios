<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales</h1>
    </section>
	<section class="content">
	    <div class="row">
	        <div class="col-xs-12">
				<div class="box box-solid">
	                <div class="box-header">
	                	<?php if ( $is_admin ): ?>
	                		<input type="hidden" id="check">
	                	<?php endif; ?>
						<h3 class="box-title">
							Reporte
						</h3>
					</div>
					<div class="box-body table-responsive">
						<table class="table table-bordered" id="oficios">
						    <thead>
							<tr>
								<th>Nombre y Clave de la Escuela</th>
								<th>Fecha</th>
								<th>Apoyos Acordados</th>
								<th>Apoyos Entregados</th>
								<th>% de Cumplimiento</th>
								<th>Observaciones</th>
							</tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	if ( isset($escuelas) ): 
						    		foreach ($escuelas as $item):?>
						    		<tr>
						    			<td><?php echo $item['nombreclave'] ?></td>
						    			<td><?php echo $item['fecha'] ?></td>
						    			<td><?php echo @$item['acordados'] ?></td>
						    			<td><?php echo $item['apoyos_entregado'] ?></td>
						    			<td><?php echo $item['avance'] ?></td>
						    			<td><?php echo $item['observaciones'] ?></td>
						    		</tr>
						    	<?php endforeach;
						    	endif; ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</aside>