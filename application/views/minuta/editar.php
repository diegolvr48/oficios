<aside class="right-side">
                <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Coordinación General de Servicios Regionales
        	<small></small></h1>
    </section>
	<section class="content">
        <?php if( validation_errors() ): ?>
        <div class="row" id="errores">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Error!</b> <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
				<div class="box box-success">
	                <div class="box-header">
						<h3 class="box-title">Minuta</h3>
					</div>
					<?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-minuta'));?>
					<div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="fecha_recepcion" class="col-sm-4 control-label">Fecha</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="fecha" name="fecha" class="form-control datemask" data-date-format="YYYY-MM-DD" value="<?php echo $minuta['fecha'] ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="folio" class="col-sm-4 control-label">Folio</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="minuta" value="<?php echo $minuta['id'] ?>">
                                    <input type="text" id="folio" name="folio" class="form-control" value="<?php echo $minuta['folio'] ?>"/><input type="hidden" value="f" name="acuerdos">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lugar" class="col-sm-2 control-label">Lugar</label>
                            <div class="col-sm-10">
                                <input type="text" id="lugar" name="lugar" class="form-control" value="<?php echo $minuta['lugar'] ?>"/>
                            </div>
                        </div>
                        <h3>Participantes</h3>
                        <hr>                    
                        <div class="form-group">
                            <label for="solicitantes" class="col-sm-2 control-label">Participantes</label>
                            <div class="col-sm-10">
                                <textarea id="solicitantes" name="solicitantes" class="form-control"><?php echo $minuta['participantes_solicitante'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ieepo" class="col-sm-2 control-label">IEEPO</label>
                            <div class="col-sm-10">
                                <textarea id="ieepo" name="ieepo" class="form-control" rows="2"><?php echo $minuta['participantes_ieepo'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="segob" class="col-sm-2 control-label">SEGOB</label>
                            <div class="col-sm-10">
                                <textarea name="segob" id="segob" class="form-control" rows="2"><?php echo $minuta['participantes_segob'] ?> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="segob" class="col-sm-2 control-label">Autoridad Municipal</label>
                            <div class="col-sm-10">
                                <textarea name="municipal" id="municipal" class="form-control" rows="2"><?php echo $minuta['autoridades_municipales'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="seccion" class="col-sm-2 control-label">Seccion XXII</label>
                            <div class="col-sm-10">
                                <textarea name="seccion" id="seccion" class="form-control" rows="2"><?php echo $minuta['participantes_seccion'] ?> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="seccion" class="col-sm-2 control-label">Descripción</label>
                            <div class="col-sm-10">
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="10"><?php echo $minuta['descripcion'] ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-2 text-right">
                                <div class="btn btn-success btn-file btn-flat archivos">
                                    <i class="fa fa-file-image-o"></i> 
                                    <p>Archivos</p>
                                    <input type="file" id="attachment"/>
                                </div>
                                <input type="hidden" name="clave" id="clave">
                                <p class="help-block">Max. 5 MB</p>
                            </div>
                            <div class="col-md-10">
                                <div class="row" id="content_img">
                                    <?php if( isset($files)):
                                    foreach($files as $file): ?>
                                        <div class="col-md-2">
                                            <div class="box box-danger">
                                                <div class="box-tools pull-right">
                                                    <button class="btn btn-danger btn-xs" type="button" data-widget="remove" data-id="<?php echo $file['id'] ?>"><i class="fa fa-times"></i></button>
                                                </div>
                                                <div class="box-body">
                                                    <?php switch ($file['type']) {
                                                        case 'application/pdf':
                                                            $file['archivo'] = 'pdf_icon.jpg';
                                                            break;
                                                    } ?>
                                                    <img src="<?php echo base_url('upload/'.$file['archivo']) ?>">
                                                </div>
                                                <div class="box-footer text-center">
                                                    <?php echo $file['name'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php  endforeach;
                                    endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-center">                        
                        <button type="submit" class="btn btn-primary btn-flat"> Guardar</button>
                        <a class="btn btn-success btn-flat" id="acuerdos"> Agregar Acuerdos</a>
                        <a href="<?php echo site_url('minutas') ?>" class="btn btn-danger btn-flat"><i class="fa fa-times"></i> Cancelar</a>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</aside>
