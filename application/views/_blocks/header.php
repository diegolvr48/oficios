<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php if (isset($page_title)) echo strip_tags($page_title) . ' | '; ?><?php echo $site_name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="Diego Olivera">

        <?php echo link_tag("assets/css/bootstrap.min.css");?>
        <?php echo link_tag("assets/css/font-awesome.min.css");?>
        <?php echo link_tag("assets/css/ionicons.css"); ?>
        <?php echo link_tag("assets/css/main.css");?>
        <?php if ( isset($css) ){
            foreach ($css as $style) {
                echo link_tag("assets/css/$style.css");
            }
        } ?>

        <link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans' rel='stylesheet' type='text/css'>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
        <?php if (isset($head_content)) echo $head_content; ?>
    </head>
    <body class="skin-blue">