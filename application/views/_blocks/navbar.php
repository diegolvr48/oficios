        <header class="header">
            <a href="<?php echo site_url() ?>" class="logo">
                <img src="<?php echo base_url('assets/img/logo.png') ?>" alt="" height="50px">
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 4 messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="<?php echo base_url('assets/img/avatar3.png') ?>" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li><!-- end message -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">Ver todos</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">1 notificacion</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="ion ion-ios-people info"></i> 5 new members joined today
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">Ver Todos</a></li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php if(isset($login_name)) echo $login_name; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo base_url('assets/img/avatar3.png') ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php if(isset($full_name)) echo $full_name; ?>
                                        <small><?php if(isset($last_login)) echo $last_login; ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Bloquear</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-default btn-flat">Salir</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url('assets/img/avatar3.png') ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Bienvenido, <?php if(isset($login_name)) echo $login_name; ?></p>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li  <?php echo $inicio ?>>
                            <a href="<?php echo site_url() ?>">
                                <i class="fa fa-dashboard"></i> <span>Inicio</span>
                            </a>
                        </li>
                        <?php if( $is_admin ): ?>
                        <li <?php echo $auth ?> >
                            <a href="<?php echo site_url('auth/') ?>">
                                <i class="fa fa-users"></i> <span>Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('minutas') ?>">
                                <i class="fa fa-list-alt"></i> <span> Minutas</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('minutas/reporte') ?>">
                                <i class="fa fa-list-alt"></i> <span> Reporte Escuela</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#help-modal">
                                <i class="fa fa-question-circle"></i><span> Ayuda</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
<div class="modal fade" id="help-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-twitch"></i> Ayuda</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Indicadores del Color en el listado</h3>
                    </div>
                    <div class="col-sm-6">
                        <div class="red indicador"></div> 
                        <p>
                            El expediante tiene 1 dia ó ya se paso de la fecha de respuesta.
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="yellow indicador"></div> 
                        <p>
                            El expediante tiene 2 ó 3 dias antes de vencer la fecha de respuesta.
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="white indicador"></div> 
                        <p>
                            El expediante tiene más de 3 dias para recibir una respuesta.
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="green indicador"></div> 
                        <p>
                            El expediente fue cerrado.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Explicación de los estados</h3>
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-left estados"><strong>Cerrado</strong></div>
                        <p>
                            El expediente se termino y ya se le dio una respuesta al solicitante.
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-left estados"><strong>Exp. Abierto</strong></div>
                        <p>
                            El expediente esta en tramite, ya se le ha dado al menos una respuesta pero continua el proceso.
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-left estados"><strong>Sin Respuesta</strong></div>
                        <p>
                            Al expediente no se le ha dado ninguna respuesta.
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal"><i class="fa fa-check-circle-o"></i> Aceptar</button>
            </div>
        </div>
    </div>
</div>