  <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
  <script src="<?php echo base_url('assets/js/app.js');?>"></script>
  <script>
      $(document).ready(function(){
        if ($(window).width() > 992) {
          $('.sidebar-toggle').trigger('click');
        }
      });
  		var base_url = "<?php  echo base_url()?>";
  </script>
  <?php if( isset($js) ):
  	foreach( $js as $script ): ?>
  	<script src="<?php echo base_url('assets/js/'.$script.'.js');?>"></script>
  <?php endforeach;
  	endif; ?>
</body>
</html>