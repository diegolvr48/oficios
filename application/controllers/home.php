<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('home_model','modelo');
        $this->load->library('form_validation');
    }

    public function index() {
        
        $this->data['view_file'] = 'home/index';
        $this->data['css']		= array('dataTables.bootstrap','dataTables.tableTools');
        $this->data['js']		= array('datatables', 'dataTables.tableTools','dataTables.bootstrap','oficios');
        $where = array();
        if ( isset($_POST) && !empty($_POST))
        {
            $data = $this->input->post(NULL, true);
            if ( $data['estado'] != '')
            {
                $where['c.status'] = $data['estado'];
                $this->data['estado_ac'] = $data['estado'];
            }
            if( $data['tiempos'] != '' )
            {
                switch ($data['tiempos']) {
                    case 1:
                        $where['DATEDIFF(fr.respuesta, NOW()) <='] = '1';
                        break;
                    case 3:
                        $where['DATEDIFF(fr.respuesta, NOW()) >='] = '2';
                        $where['DATEDIFF(fr.respuesta, NOW()) <='] = '3';
                        break;
                    case 4:
                        $where['DATEDIFF(fr.respuesta, NOW()) >'] = '3';
                        break;
                }
               $this->data['tiempo_ac'] = $data['tiempos']; 
            }
        }
        $this->data['correspondencia'] = $this->modelo->getCorrespondencia($where);
        $this->data['estados']  = $this->modelo->_get('estatus',array());
        $this->load->view('_layouts/main', $this->data);
    }

    public function nuevo()
    {
        if ( !$this->data['is_admin'] ){
            show_404();
        }
        $respuesta = false;
        $this->form_validation->set_rules('observaciones', 'Observaciones', 'xss_clean');
        $this->form_validation->set_rules('fecha_recepcion', 'Fecha Recepción', 'required');
        $this->form_validation->set_rules('hora_recepcion', 'Hora Recepción', 'required');
        $this->form_validation->set_rules('folio', 'Folio', 'required');
        $this->form_validation->set_rules('firmado', 'Firmado Por', 'required');
        $this->form_validation->set_rules('descripcion', 'Asunto', 'required');
        $this->form_validation->set_rules('tipo_documento', 'Tipo Documento', 'required');
        $this->form_validation->set_rules('area_turnar', 'Area de Atención', 'required');
        $this->form_validation->set_rules('titular_area', 'Titular', 'required');
    	$tipo_documento = $this->input->post('tipo_documento', true);
        if( $tipo_documento != 'informativo' ){
            $this->form_validation->set_rules('fecha_respuesta', 'Fecha de Respuesta', 'required');
            $respuesta = true;
        }
        if ($this->form_validation->run() == true)
        {
            $data   = array(
                    'fecha_recepcion'   => $this->input->post('fecha_recepcion', true),
                    'hora_recepcion'    => $this->input->post('hora_recepcion', true),
                    'folio'             => $this->input->post('folio', true),
                    'fecha_oficio'      => $this->input->post('fecha_oficio', true),
                    'firmado'           => $this->input->post('firmado', true),
                    'descripcion'       => $this->input->post('descripcion', true),
                    'tipo_documento'    => $tipo_documento,
                    'area_turnar'       => $this->input->post('area_turnar', true),
                    'titular_area'      => $this->input->post('titular_area', true),
                    'observaciones'     => $this->input->post('observaciones', true),
                ); 
            if( $respuesta ){
                $data_respuesta['respuesta'] = $this->input->post('fecha_respuesta', true);
            }
            else{
                $data_respuesta['respuesta'] = $data['fecha_recepcion'];
                $data['status']     = 3;
            }

            if ($folio      = $this->modelo->newCorrespondencia($data))
            {
                if( $minuta = $this->input->post('minuta', true) )
                {
                    $this->session->set_flashdata('oficio', $folio);
                }
                if( $folioMinuta = $this->input->post('folioMinuta', true) )
                {
                    $this->modelo->_set('minuta_oficio', array('minuta_id' => $folioMinuta, 'oficio_id' => $folio));
                }
                $clave  = $this->input->post('clave', true);
                $this->modelo->updateImg('multimedia_correspondencia',array('clave' => $clave, 'correspondencia_id' => $folio));
                $data_respuesta['correspondencia_id'] = $folio;
                $this->modelo->setFechaRespuesta($data_respuesta);
            }
            if( isset($minuta) && $minuta)
            {
                redirect('minutas/nuevo', 'refresh');
            }else
            {
                redirect('/','refresh');
            }
        }else{
            $this->data['view_file'] = 'home/nuevo';
            $this->data['groups']   = $this->ion_auth->groups()->result(); 
        	$this->data['users']    = $this->ion_auth->users()->result();
            $this->data['css']      = array('bootstrap-datetimepicker.min','jasny-bootstrap.min');
            $this->data['js']		= array('jasny-bootstrap.min','moment','bootstrap-datetimepicker.min','nuevo_oficio');
            $this->load->view('_layouts/main', $this->data);
        }
    }

    public function editar($folio_oficio)
    {
        if ( !isset($folio_oficio) || !is_numeric($folio_oficio))
        {
            show_404();
        }
        $respuesta = false;
        $this->form_validation->set_rules('id_correspondencia', 'Folio Correspondencia', 'required');
        $this->form_validation->set_rules('fecha_recepcion', 'Fecha Recepción', 'required');
        $this->form_validation->set_rules('hora_recepcion', 'Hora Recepción', 'required');
        $this->form_validation->set_rules('folio', 'Folio', 'required');
        $this->form_validation->set_rules('firmado', 'Firmado Por', 'required');
        $this->form_validation->set_rules('descripcion', 'Asunto', 'required');
        $this->form_validation->set_rules('tipo_documento', 'Tipo Documento', 'required');
        $this->form_validation->set_rules('area_turnar', 'Area de Atención', 'required');
        $this->form_validation->set_rules('titular_area', 'Titular', 'required');
        $tipo_documento = $this->input->post('tipo_documento', true);
        if( $tipo_documento != 'informativo' ){
            $this->form_validation->set_rules('fecha_respuesta', 'Fecha de Respuesta', 'required');
            $respuesta = true;
            
        }
        if ($this->form_validation->run() == true)
        {
            $data   = array(
                    'fecha_recepcion'   => $this->input->post('fecha_recepcion', true),
                    'hora_recepcion'    => $this->input->post('hora_recepcion', true),
                    'folio'             => $this->input->post('folio', true),
                    'fecha_oficio'      => $this->input->post('fecha_oficio', true),
                    'firmado'           => $this->input->post('firmado', true),
                    'descripcion'       => $this->input->post('descripcion', true),
                    'tipo_documento'    => $tipo_documento,
                    'area_turnar'       => $this->input->post('area_turnar', true),
                    'titular_area'      => $this->input->post('titular_area', true),
                    'observaciones'     => $this->input->post('observaciones', true),
                ); 
            if( $respuesta )
                $data_respuesta['respuesta'] = $this->input->post('fecha_respuesta', true);
            else{
                $data_respuesta['respuesta']  = $data['fecha_recepcion'];
                $data['status']     = 3;
            }
            $id = $this->input->post('id_correspondencia', true);
            if ( $id  == $folio_oficio){
                $folio      = $this->modelo->editCorrespondencia($id, $data);
                $clave  = $this->input->post('clave', true);
                $this->modelo->updateImg('multimedia_correspondencia',array('clave' => $clave, 'correspondencia_id' => $folio));
                $data_respuesta['correspondencia_id'] = $id;
                $this->modelo->setFechaRespuesta($data_respuesta, true);
                redirect('/','refresh');
            }
            else{
                show_500();
            }
        }else{

            $this->data['view_file'] = 'home/editar';
            $this->data['datos']    = $this->modelo->get('correspondencia',array('id'=>$folio_oficio));
            $this->data['respuesta']= $this->modelo->get('fecha_respuesta',array('correspondencia_id'=>$folio_oficio));
            $this->data['groups']   = $this->ion_auth->groups()->result();
            $this->data['users']    = $this->ion_auth->users($this->data['datos']['area_turnar'])->result(); 
            $this->data['css']      = array('bootstrap-datetimepicker.min','jasny-bootstrap.min');
            $this->data['js']       = array('jasny-bootstrap.min','moment','bootstrap-datetimepicker.min','nuevo_oficio');
            $this->data['files']   = $this->modelo->_get('multimedia_correspondencia',array('correspondencia_id' => $folio_oficio));
            $this->load->view('_layouts/main', $this->data);
        }
    }

    public function ver($folio_oficio)
    {
        if ( !isset($folio_oficio) || !is_numeric($folio_oficio))
        {
            show_404();
        }

        $this->form_validation->set_rules('oficio', '', 'required');
        $this->form_validation->set_rules('titulo', 'Descripcion', 'required');
        $this->form_validation->set_rules('respuesta', 'Detalle', 'required');
        if( isset($_POST) && !empty($_POST) )
        {
            $folio = $this->input->post('oficio', true);
            if ($this->form_validation->run() === TRUE)
            {
                if( $folio == $folio_oficio )
                {
                    $data = array(
                        'folio'     => $folio,
                        'titulo'    => $this->input->post('titulo', true),
                        'respuesta' => $this->input->post('respuesta', true)
                        );
                    $terminado = $this->input->post('terminado', true);
                    $respuesta['correspondencia_id'] = $folio;
                    if( $terminado == 'no' )
                    {
                        $status =array('status'=>2);
                        $respuesta['respuesta'] = $this->input->post('fecha_respuesta', true);
                        if( $respuesta['respuesta'] == '' )
                            $respuesta['respuesta'] = date("Y-m-d",strtotime("+1 day"));
                        $this->modelo->setFechaRespuesta($respuesta, true);
                    }elseif($terminado == 'si'){
                        $respuesta['respuesta'] = date("Y-m-d");
                        $this->modelo->setFechaRespuesta($respuesta, true);
                        $status =array('status'=>3);
                    }else{
                        $status = NULL;
                    }
                    $id_resp = $this->modelo->setRespuesta($data, $status);
                    $clave  = $this->input->post('clave', true);
                    $this->modelo->updateImg('multimedia_respuestas',array('clave' => $clave, 'respuesta_id' => $id_resp));
                }
            }
        }
        $this->load->helper('formato');
        $this->data['view_file'] = 'home/ver';
        $this->data['css']      = array('bootstrap-datetimepicker.min');
        $this->data['js']       = array('moment','bootstrap-datetimepicker.min','ver');
        $this->data['datos']    = $this->modelo->get('correspondencia',array('id'=>$folio_oficio));
        $this->data['group']   = $this->ion_auth->group($this->data['datos']['area_turnar'])->row();
        $this->data['user']    = $this->ion_auth->user($this->data['datos']['titular_area'])->row();
        $this->data['minuta'] = $this->modelo->get('minuta_oficio', array('oficio_id' => $folio_oficio));
        $this->data['files']   = $this->modelo->_get('multimedia_correspondencia',array('correspondencia_id' => $folio_oficio));
        $this->data['respuestas']= $this->modelo->_get('respuestas',array('folio'=>$folio_oficio)); 
        foreach ($this->data['respuestas'] as $index => $respuesta) {
            $this->data['respuestas'][$index]['files'] = $this->modelo->_get('multimedia_respuestas',array('respuesta_id'=>$respuesta['id'])); 
        }
        $this->load->view('_layouts/main', $this->data);
    }
    public function titulares()
    {
        $data = $this->input->post(NULL, true);
        if( isset($data['area']) && $data['area'] )
        {
            $users = $this->ion_auth->users(array($data['area']))->result();
            echo json_encode(array('status' => 'OK', 'users' => $users));
        }else{
            echo json_encode(array('status' => 'ERROR'));
        }
    }

    public function bajacorrespondencia()
    {
        if ( isset($_POST) && !empty($_POST) )
        {
            $data['correspondencia_id'] = $this->input->post('correspondencia');
            if( $data['correspondencia_id'] )
            {
                if( $this->modelo->deleteCorrespondencia($data) )
                {
                    echo json_encode(array('mensaje' => 'Borrado Exitosamente'));
                }else{
                    echo json_encode(array('mensaje' => 'ERROR'));
                }

            }else{
                echo json_encode(array('mensaje' => 'ERROR'));
            }
        }else{
            echo json_encode(array('mensaje' => 'ERROR'));
        }
    }

    public function imagenes($respuesta=null)
    {
        $clave = $this->input->post('clave', true);
        if ( !isset($clave) || $clave == '' )
        {
            $clave = random_string('unique');
        }
        $name       = explode('.', $_FILES['attachment']['name']);
        $type       = $_FILES['attachment']['type'];
        $name_file  = random_string('unique').".".$name[count($name)-1];
        $sourcePath = $_FILES['attachment']['tmp_name'];
        $targetPath = $_SERVER['DOCUMENT_ROOT']."/oficios/upload/".$name_file;
        if ( move_uploaded_file($sourcePath,$targetPath) )
        {
            if ( isset($respuesta) ){
                $id = $this->modelo->insertImg('multimedia_respuestas', array('archivo' => $name_file, 'type' => $type, 'clave' => $clave, 'name' => $name[0]));
            }else
            {
                $id = $this->modelo->insertImg('multimedia_correspondencia', array('archivo' => $name_file, 'type' => $type, 'clave' => $clave, 'name' => $name[0]));
            }
            switch ($type) {
                case 'application/pdf':
                    $name_file = 'pdf_icon.jpg';
                    break;
            }
            echo json_encode(array('status' => 'OK', 'clave' => $clave, 'name' => $name_file, 'imagen' => $id));
        }else{
            echo json_encode(array('status' => 'ERROR', 'clave' => $clave, 'name' => ''));
        }

    }

    public function removeImg($respuesta=null)
    {
        if ( isset($_POST) && !empty($_POST) )
        {
            $imagen = $this->input->post('imagen', true);
            if(isset($respuesta))
            {
                $this->modelo->removeImg('multimedia_respuestas', $imagen);
            }else
            { 
                $this->modelo->removeImg('multimedia_correspondencia', $imagen);
            }
            echo json_encode(array('status' => 'OK'));
        }else{
            echo json_encode(array('status' => 'ERROR'));
        }
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */