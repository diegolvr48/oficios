<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Minutas extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('minuta_model','modelo');
        $this->load->library('form_validation');
        $this->load->helper(array('progress','formato'));
    }

    public function index() {
    	$this->data['view_file'] = 'minuta/index';
        $this->data['css']		= array('dataTables.bootstrap','dataTables.tableTools');
        $this->data['js']		= array('datatables', 'dataTables.tableTools','dataTables.bootstrap','minutas');
        $this->data['minutas'] = $this->modelo->getMinutas();
        $this->load->view('_layouts/main', $this->data);
    }

    public function nuevo()
    {
        if ( !$this->data['is_admin'] ){
            show_404();
        }
        $respuesta = false;
        $this->form_validation->set_rules('fecha', 'Fecha', 'required');
        $this->form_validation->set_rules('isEscuela', 'Es escuela', 'xss_clean');
        $this->form_validation->set_rules('nombreEscuela', 'Nombre Escuela', 'xss_clean');
        $this->form_validation->set_rules('claveEscuela', 'Clave escuela', 'xss_clean');
        $this->form_validation->set_rules('segob', 'SEGOB', 'xss_clean');
        $this->form_validation->set_rules('oficos', 'Oficio', 'xss_clean');
        $this->form_validation->set_rules('clave', 'Clave', 'xss_clean');
        $this->form_validation->set_rules('seccion', 'Seccion', 'xss_clean');
        $this->form_validation->set_rules('municipal', 'Municipal', 'xss_clean');
        $this->form_validation->set_rules('folio', 'Folio', 'required');
        $this->form_validation->set_rules('descripcion', 'Problematica', 'required');
        $this->form_validation->set_rules('lugar', 'Lugar', 'required');
        $this->form_validation->set_rules('solicitantes', 'Participantes', 'required');
        $this->form_validation->set_rules('ieepo', 'Participantes', 'required');
        
        if ($this->form_validation->run() === true)
        {
            $data   = array(
                    'fecha'   => $this->input->post('fecha', true),
                    'folio'    => $this->input->post('folio', true),
                    'descripcion'             => $this->input->post('descripcion', true),
                    'lugar'      => $this->input->post('lugar', true),
                    'participantes_solicitante'           => $this->input->post('solicitantes', true),
                    'participantes_ieepo'       => $this->input->post('ieepo', true),
                    'participantes_segob'    => $this->input->post('segob', true),
                    'participantes_seccion'       => $this->input->post('seccion', true),
                    'autoridades_municipales'   => $this->input->post('municipal', true),
                    'claveEscuela'      => $this->input->post('claveEscuela', true),
                    'nombreEscuela'      => $this->input->post('nombreEscuela', true),
                    'isEscuela'      => $this->input->post('isEscuela', true),
                ); 

            if ( $folio = $this->modelo->newMinuta($data) )
            {
                $this->modelo->updateImg('multimedia_minuta',array('minuta_id' => $folio, 'clave' => $this->input->post('clave',true)));
                if( $oficio = $this->input->post('oficio', true) )
               {
                    $this->modelo->_set('minuta_oficio', array('minuta_id' => $folio, 'oficio_id' => $oficio));
               }
               if( $this->input->post('acuerdos', TRUE) == 't' )
               {
            		redirect('minutas/nuevo_acuerdo/'.$folio, 'refresh');
               }else{
               		redirect('minutas', 'refresh');
               }
            }else{
            	show_500();
            }
            
        }else{
            if( $oficio = $this->input->post('oficio', true) )
            {
                $this->session->set_flashdata('oficio', $oficio);
            }
            $this->data['view_file'] = 'minuta/nuevo';
            $this->data['css']      = array('bootstrap-datetimepicker.min','selectize.bootstrap3');
            $this->data['js']		= array('moment','bootstrap-datetimepicker.min', 'selectize', 'nuevo_minuta');
            $this->load->view('_layouts/main', $this->data);
        }
    }

    public function nuevo_acuerdo($folio, $id = null)
    {
        if(!isset($folio) || !is_numeric($folio))
        {
            redirect('minutas/nuevo', 'refresh');
        }
        if( isset($_POST) && !empty($_POST) )
        {
            $this->form_validation->set_rules('minuta', 'Folio Minuta', 'required');
            $this->form_validation->set_rules('folio', 'Folio', 'required');
            $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
            if ($this->form_validation->run() === true)
            {
                $datos = array(
                    'folio'        => $this->input->post('folio',true),
                    'descripcion'  => $this->input->post('descripcion', true),
                    'minuta_id'    => $this->input->post('minuta', true) 
                    );
                $acuerdo_id = $this->input->post('folio_acuerdo', true);
                if( isset($acuerdo_id) && is_numeric($acuerdo_id) && $acuerdo_id){
                    $acuerdo = $acuerdo_id;
                    $this->modelo->update('acuerdos', $datos, $acuerdo);
                }else{
                    $acuerdo = $this->modelo->_set('acuerdos', $datos);
                }
                if ( isset($acuerdo) )
                {
                    $this->data['acuerdo_id'] = $acuerdo;
                    $this->data['folio_acuerdo'] = $this->input->post('folio',true);
                }
            }
            if( $minuta = $this->input->post('minuta', true) && $this->input->post('oficio', true) )
            {
                $this->session->set_flashdata('minuta', $minuta);
                redirect('home/nuevo', 'refresh');
            }
            if($this->input->post('terminado', true) == 't'){
                redirect('minutas', 'refresh');
            }
        }elseif( isset($id) && is_numeric($id) ){
            $this->data['acuerdo_data'] = $this->modelo->_get('acuerdos',array('id'=> $id));
        }
    	$this->data['view_file'] = 'minuta/acuerdos';
        $this->data['css']      = array('bootstrap-datetimepicker.min','selectize.bootstrap3');
        $this->data['js']		= array('moment','bootstrap-datetimepicker.min', 'acuerdos');
        $this->data['minuta']   = $folio;
        $this->data['acuerdos_list'] = $this->modelo->get('acuerdos', array('minuta_id' => $folio));
        $this->load->view('_layouts/main', $this->data);
    }

    public function accion()
    {
        $this->form_validation->set_rules('value', 'Descripcion', 'required|trim|xss_clean');
        $this->form_validation->set_rules('folio', 'Folio', 'required|trim|xss_clean');
        $this->form_validation->set_rules('acuerdo', 'Acuerdo', 'required|trim|xss_clean');
        $this->form_validation->set_rules('folio_accion', 'Folio Accion', 'trim|xss_clean');
        
        if($this->form_validation->run() === true)
        {
            $data = $this->input->post(NULL, true);
            if( $data['acuerdo'] == $data['folio']){
                $datos = array('descripcion' => $data['value'], 'acuerdo_id' => $data['acuerdo']);
                if(isset($data['folio_accion']) && !empty($data['folio_accion']))
                {
                    $folio = $data['folio_accion'];
                    $this->modelo->update('acciones', $datos, $folio);
                    $update = 1;
                }else{
                    $num_accion = $this->modelo->get_max($data['folio']);
                    $datos['folio'] = $num_accion;
                    $folio = $this->modelo->_set('acciones', $datos);                
                    $update = 0;
                }
                echo json_encode(array('status' => 'OK','folio'=> $folio, 'update' => $update));
            }else{
                echo json_encode(array('status' => 'ERROR', 'mensaje' => ''));
            }
        }else{
            echo json_encode(array('status' => 'ERROR','mensaje' => validation_errors()));
        }
    }

    public function acciones()
    {
        $acuerdo = $this->input->post('acuerdo', true);
        if ( $acuerdo )
        {
            $data   = $this->modelo->_get('acuerdos', array('id' => $acuerdo));
            $acciones = $this->modelo->get('acciones', array('acuerdo_id' => $acuerdo));
            echo json_encode(array('status' => 'OK', 'data' => $acciones, 'acuerdo' => $data));
        }else{
            echo json_encode(array('status' => 'ERROR','mensaje' => ''));
        }
    }

    public function ver($minuta)
    {
        if(!isset($minuta) || !is_numeric($minuta)){
            show_404();
        }
        if( isset($_POST) && !empty($_POST))
        {
           
            $this->form_validation->set_rules('minuta', 'minuta', 'required|trim|xss_clean');
            
            if($this->form_validation->run() === true)
            {
                $data = array(
                    'apoyos_entregado' => $this->input->post('entregado', true),
                    'avance'            => $this->input->post('avance', true),
                    'observaciones'     => $this->input->post('observaciones', true)
                    );
                $this->modelo->update('minuta', $data, $this->input->post('minuta', true));
            }
        }
        $this->data['view_file'] = 'minuta/ver';
        $this->data['css']      = array('dataTables.bootstrap','dataTables.tableTools');
        $this->data['js']       = array('datatables', 'dataTables.tableTools','dataTables.bootstrap','minuta_ver');
        $this->data['minuta']   = $this->modelo->_get('minuta', array('id' => $minuta));
        $this->data['acuerdos'] = $this->modelo->get('acuerdos', array('minuta_id' => $minuta));
        $this->data['oficios'] = $this->modelo->get('minuta_oficio', array('minuta_id' => $minuta));
        
        $this->data['files']    =  $this->modelo->get('multimedia_minuta', array('minuta_id' => $minuta));
        $this->load->view('_layouts/main', $this->data);
    }

    public function accion_hecha()
    {
        $accion = $this->input->post('accion', true);
        if ( $accion )
        {
            if( $this->modelo->update('acciones', array('status' => 1), $accion) )
            {
                $acuerdo = $this->modelo->_get('acciones', array('id' => $accion));
                echo json_encode(array('status' => 'OK','progress' => acuerdo_progress($acuerdo['acuerdo_id']), 'acuerdo' => $acuerdo['acuerdo_id']));
            }else{
                echo json_encode(array('status' => 'ERROR'));
            }
        }else{
            echo json_encode(array('status' => 'ERROR','mensaje' => ''));
        }
    }

    public function editar($minuta)
    {
        if(!isset($minuta) || !is_numeric($minuta) || !$this->data['is_admin']){
            show_404();
        }
        $this->form_validation->set_rules('fecha', 'Fecha', 'required');
        $this->form_validation->set_rules('folio', 'Folio', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
        $this->form_validation->set_rules('lugar', 'Lugar', 'required');
        $this->form_validation->set_rules('solicitantes', 'Participantes', 'required');
        $this->form_validation->set_rules('minuta', 'Numero Minuta', 'required');
        if ($this->form_validation->run() === true)
        {
            $data   = array(
                    'fecha'   => $this->input->post('fecha', true),
                    'folio'    => $this->input->post('folio', true),
                    'descripcion'             => $this->input->post('descripcion', true),
                    'lugar'      => $this->input->post('lugar', true),
                    'participantes_solicitante'           => $this->input->post('solicitantes', true),
                    'participantes_ieepo'       => $this->input->post('ieepo', true),
                    'participantes_segob'    => $this->input->post('segob', true),
                    'participantes_seccion'       => $this->input->post('seccion', true),
                ); 
            $folio = $this->input->post('minuta', true);
            if ( $this->modelo->update('minuta',$data, $folio) )
            {
                $this->modelo->updateImg('multimedia_minuta',array('minuta_id' => $folio, 'clave' => $this->input->post('clave',true)));
               if( $this->input->post('acuerdos', TRUE) == 't' )
               {
                    redirect('minutas/nuevo_acuerdo/'.$folio, 'refresh');
               }else{
                    redirect('minutas', 'refresh');
               }
            }else{
                show_500();
            }
            
        }else{
            $this->data['view_file'] = 'minuta/editar';
            $this->data['minuta']   = $this->modelo->_get('minuta', array('id' => $minuta));
            $this->data['files']    = $this->modelo->get('multimedia_minuta', array('minuta_id' => $minuta));
            $this->data['css']      = array('bootstrap-datetimepicker.min','selectize.bootstrap3');
            $this->data['js']       = array('moment','bootstrap-datetimepicker.min', 'selectize', 'editar_minuta');
            $this->load->view('_layouts/main', $this->data);
        }
    }

    public function minuta_remove()
    {
        $minuta = $this->input->post('minuta', true);
        if( $minuta )
        {
            $this->modelo->removeMinuta($minuta);
            echo json_encode(array('status' => 'OK', 'mensaje' => 'Minuta eliminada con exito'));
        }else{
            echo json_encode(array('status' => 'ERROR', 'mensaje' => 'Error al Eliminar'));
        }
    }
    public function imagenes($respuesta=null)
    {
        $clave = $this->input->post('clave', true);
        if ( !isset($clave) || $clave == '' )
        {
            $clave = random_string('unique');
        }
        $name       = explode('.', $_FILES['attachment']['name']);
        $type       = $_FILES['attachment']['type'];
        $name_file  = random_string('unique').".".$name[count($name)-1];
        $sourcePath = $_FILES['attachment']['tmp_name'];
        $targetPath = $_SERVER['DOCUMENT_ROOT']."/oficios/upload/".$name_file;
        
        if ( move_uploaded_file($sourcePath,$targetPath) )
        {
            if ( isset($respuesta) ){
                //$id = $this->modelo->insertImg('multimedia_respuestas', array('archivo' => $name_file, 'type' => $type, 'clave' => $clave, 'name' => $name[0]));
            }else
            {
                $id = $this->modelo->insertImg('multimedia_minuta', array('archivo' => $name_file, 'type' => $type, 'clave' => $clave, 'name' => $name[0]));
            }
            switch ($type) {
                case 'application/pdf':
                    $name_file = 'pdf_icon.jpg';
                    break;
            }
            echo json_encode(array('status' => 'OK', 'clave' => $clave, 'name' => $name_file, 'imagen' => $id));
        }else{
            print_r($_FILES);
            echo json_encode(array('status' => 'ERROR', 'clave' => $clave, 'name' => '', 'path' => $targetPath));
        }

    }

    public function removeImg($respuesta=null)
    {
        if ( isset($_POST) && !empty($_POST) )
        {
            $imagen = $this->input->post('imagen', true);
            if(isset($respuesta))
            {
                //$this->modelo->removeImg('multimedia_respuestas', $imagen);
            }else
            { 
                $this->modelo->removeImg('multimedia_minuta', $imagen);
            }
            echo json_encode(array('status' => 'OK'));
        }else{
            echo json_encode(array('status' => 'ERROR'));
        }
    }

    public function acuerdo_remove()
    {
        $acuerdo    = $this->input->post('acuerdo', true);
        if( $acuerdo ){
            $this->modelo->removeAcuerdo($acuerdo);
            echo json_encode(array('status' => 'OK'));
        }else{
            echo json_encode(array('status' => 'ERROR'));
        }
    }

    public function accion_remove()
    {
        $accion    = $this->input->post('accion', true);
        if( $accion ){
            $this->modelo->removeAccion($accion);
            echo json_encode(array('status' => 'OK'));
        }else{
            echo json_encode(array('status' => 'ERROR'));
        }
    }

    public function acuerdo()
    {
        echo json_encode($this->modelo->_get('acuerdos', array('id' => $this->input->post('acuerdo',true))));
    }

    public function accion_edit()
    {
        echo json_encode($this->modelo->_get('acciones', array('id' => $this->input->post('accion',true))));
    }

    public function reporte()
    {
        $this->data['view_file'] = 'minuta/reporte';
        $this->data['css']      = array('dataTables.bootstrap','dataTables.tableTools');
        $this->data['js']       = array('datatables', 'dataTables.tableTools','dataTables.bootstrap','reporte');
        $this->data['escuelas'] = $this->modelo->getEscuela();
        $this->load->view('_layouts/main', $this->data);
    }
}