<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('minuta_progress'))
{
	function minuta_progress($minuta)
	{
		if( $minuta == 0){
			return 'class="alert alert-danger"';
		}elseif( $minuta > 0 && $minuta < 100 ){
			return 'class="alert alert-orange"';
		}elseif( $minuta == 100){
			return 'class="alert alert-success"';;
		}else{
			return '';
		}
	}
}
if ( ! function_exists('acuerdo_progress'))
{
	function acuerdo_progress($acuerdo)
	{
		$CI =& get_instance();
		$CI->db->where(array('acuerdo_id'=> $acuerdo,'status' => 1));
		$CI->db->from('acciones');
		$acciones_hechas = $CI->db->count_all_results();
		$CI->db->where('acuerdo_id', $acuerdo);
		$CI->db->from('acciones');
		$acciones_totales = $CI->db->count_all_results();
		if( $acciones_totales > 0 ){
			$porcentaje = ($acciones_hechas/$acciones_totales)*100;
		}else{
			$porcentaje = 100;
		}
		return number_format($porcentaje,2);
	}
}