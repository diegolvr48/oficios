<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('format_fecha'))
{
	function format_fecha($fecha)
	{
		$data = explode(' ', $fecha);
		return $data[0];
	}
}
if ( ! function_exists('format_file'))
{
	function format_file($type)
	{
		switch ($type) {
			case 'image/jpeg':
			case 'image/png':
				return '<i class="fa fa-file-image-o"></i>';
			case 'application/pdf':
				return '<i class="fa fa-file-pdf-o"></i>';
			default:
				return '<i class="fa fa-file-o"></i>';
		}
	}
}