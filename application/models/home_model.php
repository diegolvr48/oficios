<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
	public function newCorrespondencia($data)
	{
		if ( $this->db->insert('correspondencia',$data) )
			return $this->db->insert_id();
		else
			return 0;
	}

	public function editCorrespondencia($id, $data)
	{
		$this->db->where('id', $id);
		if ( $this->db->update('correspondencia',$data) )
			return $id;
		else
			return 0;
	}

	public function getCorrespondencia($where)
	{
		if( !$this->ion_auth->is_admin() )
		{
			$areas = $this->ion_auth->get_users_groups()->result_array();
			$areas_res = array();
			foreach ($areas as $area) {
				$areas_res[] = $area['id'];
			}
			$user = $this->ion_auth->user()->row();
			$this->db->where_in('c.area_turnar', $areas_res);
			$this->db->where('c.titular_area',$user->id);
		}
		if(isset($where) && count($where) > 0)
		{
			$this->db->where($where);
		}
		$this->db->select('c.id, c.fecha_recepcion, c.folio, c.descripcion, CONCAT(u.last_name," ",u.first_name) as nombre_titular, c.fecha_oficio, DATEDIFF(fr.respuesta, NOW()) dias_faltantes, c.status, fr.respuesta, e.nombre estado',false);
		$this->db->join('fecha_respuesta fr', 'c.id = fr.correspondencia_id');
		$this->db->join('users u', 'u.id = c.titular_area');
		$this->db->join('estatus e', 'c.status = e.id');
		$query = $this->db->get('correspondencia c');
		return $query->result_array();
	}

	public function get($table, $where)
	{
		if($this->db->table_exists($table))
		{
			$row = $this->db->get_where($table, $where);
			return $row -> row_array();
		}else
		{
			return FALSE;
		}
	}

	public function _get($table, $where)
	{
		if($this->db->table_exists($table))
		{
			$row = $this->db->get_where($table, $where);
			return $row -> result_array();
		}else
		{
			return FALSE;
		}
	}

	public function setRespuesta($data, $status)
	{
		if ( isset($status) && isset($status['status']))
		{
			$this->db->where('id', $data['folio']);
			$this->db->update('correspondencia', $status);
		}
		$this->db->insert('respuestas', $data);
		return $this->db->insert_id();
	}

	public function setFechaRespuesta($data, $update=false)
	{
		if ( $update === false )
		{
			$this->db->insert('fecha_respuesta', $data);
			return $this->db->insert_id();
		}else{
			$this->db->where('correspondencia_id', $data['correspondencia_id']);
			return $this->db->update('fecha_respuesta', $data);
		}
	}

	public function deleteCorrespondencia($data)
	{
		$this->db->where('id', $data['correspondencia_id']);
		return $this->db->delete('correspondencia');
	}

	public function insertImg($tabla,$data)
	{
		$this->db->insert($tabla, $data);
		return $this->db->insert_id();
	}

	public function removeImg($tabla,$id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($tabla);
	}

	public function updateImg($tabla, $data)
	{
		$this->db->where('clave', $data['clave']);
		return $this->db->update($tabla, $data);
	}

	public function _set($table, $data)
	{
		if ( $this->db->table_exists($table) )
		{
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}else{
			return NULL;
		}
		
	}
}