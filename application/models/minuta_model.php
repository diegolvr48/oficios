<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Minuta_model extends CI_Model {

	public function getMinutas()
	{
		$query = $this->db->get('minuta');
		return $query->result_array();
	}

	public function newMinuta($data)
	{
		$this->db->insert('minuta', $data);
		return $this->db->insert_id();
	}

	public function _set($table, $data)
	{
		if ( $this->db->table_exists($table) )
		{
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}else{
			return NULL;
		}
		
	}

	public function get($table, $where)
	{
		if( isset($where) && is_array($where) )
			$this->db->where($where);
		$query = $this->db->get($table);
		if( $query ->num_rows() > 0 )
		{
			return $query->result_array();
		}
		else{
			return NULL;
		}
	}
	public function _get($table, $where)
	{
		if( isset($where) && is_array($where) )
			$this->db->where($where);
		$query = $this->db->get($table);
		if( $query ->num_rows() > 0 )
		{
			return $query->row_array();
		}
		else{
			return NULL;
		}
	}
	public function get_max($acuerdo)
	{
		$this->db->select_max('folio');
		$this->db->where('acuerdo_id', $acuerdo);
		$query = $this->db->get('acciones');
		if($query -> num_rows() > 0){
			$num = $query->row()->folio;
			return isset($num)?($num+1):1;
		}else{
			return 1;
		}
	}

	public function update($table, $data, $id)
	{
		if ( $this->db->table_exists($table) )
		{
			$this->db->where('id', $id);
			return $this->db->update($table, $data);
		}else{
			return false;
		}
	}

	public function insertImg($tabla,$data)
	{
		$this->db->insert($tabla, $data);
		return $this->db->insert_id();
	}

	public function removeImg($tabla,$id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($tabla);
	}

	public function updateImg($tabla, $data)
	{
		$this->db->where('clave', $data['clave']);
		return $this->db->update($tabla, $data);
	}

	public function removeAcuerdo($acuerdo)
	{
		$this->db->where('acuerdo_id', $acuerdo);
		$this->db->delete('acciones');
		$this->db->where('id', $acuerdo);
		return $this->db->delete('acuerdos');
	}

	public function removeAccion($accion)
	{
		$this->db->where('id', $accion);
		return $this->db->delete('acciones');
	}

	public function removeMinuta($minuta)
	{
		$acuerdos = $this->db->get_where('acuerdos', array('minuta_id'=>$minuta))->result_array();
		foreach ($acuerdos as $acuerdo) {
			$this->db->where('acuerdo_id', $acuerdo['id']);
			$this->db->delete('acciones');
		}
		$this->db->where('minuta_id', $minuta);
		$this->db->delete('acuerdos');
		$this->db->where('id', $minuta);
		return $this->db->delete('minuta');
	}

	public function getEscuela()
	{
		$this->db->select("CONCAT(nombreescuela, ' ','Clave ',claveescuela) nombreclave, fecha, apoyos_entregado, avance, observaciones", FALSE);
		$this->db->where('isescuela', 'SI');
		$query = $this->db->get('minuta');
		return $query->result_array();
	}
}